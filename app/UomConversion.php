<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UomConversion extends Model
{
    protected $hidden = [
        'updated_at', 'created_at', 'pivot', 'deleted_at',
    ];

    public function fromUnitOfMeasurment(){
        return $this->belongsTo(Uom::class,'uom_from','id');
    }

    public function toUnitOfMeasurment(){
        return $this->belongsTo(Uom::class,'uom_to','id');
    }
}
