<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrganizationType extends Model
{
    public function organizations(){
        return $this->hasMany(Organization::class, 'id', 'organization_type_id');
    }
}
