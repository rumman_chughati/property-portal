<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prodcat extends Model
{
    protected $hidden = [
        'updated_at', 'created_at', 'deleted_at','pivot'
    ];

    public function portal(){
        return $this->belongsToMany(Portal::class);
    }

    public function attributes(){
        return $this->hasMany(ProdcatAttribute::class);
    }

    public function prodcatParent(){
        return $this->belongsTo(Prodcat::class,'parent_id','id');
    }

}
