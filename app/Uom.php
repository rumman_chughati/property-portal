<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Uom extends Model
{
    protected $hidden = [
        'updated_at', 'created_at', 'pivot', 'deleted_at',
    ];

    public function portal(){
        return $this->belongsToMany(Portal::class);
    }
}
