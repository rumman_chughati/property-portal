<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyClass extends Model
{

    public function subTypes(){
        return $this->hasMany(PropertyClass::class, 'parent_id', 'id');
    }
}
