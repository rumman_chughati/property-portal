<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phase extends Model
{
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at',
    ];

    public function scheme(){
        return $this->belongsTo(Scheme::class);
    }

    public function blocks(){
        return $this->hasMany(Block::class);
    }
}
