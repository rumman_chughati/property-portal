<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    function propertyFeature(){
        return $this->hasMany(PropertyFeature::class);
    }

    public function propertySubfeatures() {
        return $this->hasMany(Feature::class, 'parent_id', 'id');
    }

    public function propertyType() {
        return $this->belongsTo(PropertyType::class, 'property_type_id', 'id');
    }
}
