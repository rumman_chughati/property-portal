<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyFeature extends Model
{
    function property(){
        return $this->belongsTo(Property::class, 'property_id', 'id');
    }

    function feature(){
        return $this->belongsTo(Feature::class, 'feature_id', 'id');
    }
}
