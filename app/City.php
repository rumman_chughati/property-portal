<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at',
    ];

    public function state(){
        return $this->belongsTo(State::class);
    }

    public function organization_locations(){
        return $this->hasMany(OrganizationLocation::class);
    }

    public function schemes(){
        return $this->hasMany(Scheme::class);
    }
}
