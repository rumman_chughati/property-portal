<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function approvedByUser(){
        return $this->belongsTo(User::class, 'approved_by', 'id');
    }

    public function propertyFeature(){
        return $this->hasMany(PropertyFeature::class);
    }

    public function bid(){
        return $this->hasMany(Bid::class);
    }
}
