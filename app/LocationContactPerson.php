<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationContactPerson extends Model
{
    public function organization(){
        return $this->belongsTo(Organization::class, 'organization_id', 'id');
    }

    public function organization_location(){
        return $this->belongsTo(OrganizationLocation::class, 'organization_location_id', 'id');
    }
}
