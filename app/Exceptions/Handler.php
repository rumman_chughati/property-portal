<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Symfony\Component\HttpKernel\Tests\Exception\UnauthorizedHttpExceptionTest;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if($exception instanceof TokenExpiredException){
            return response()->json([
               'message' => "token expired",
               'status_code' => JsonResponse::HTTP_UNAUTHORIZED,
            ]);
        }
        if($exception instanceof TokenInvalidException) {
            return response()->json([
                'message' => "token is invalid",
                'status_code' => JsonResponse::HTTP_UNAUTHORIZED,
            ]);
        }
        if($exception instanceof UnauthorizedException){
            return response()->json([
                'message' => "your are not authorized",
                'status_code' => JsonResponse::HTTP_UNAUTHORIZED,
            ]);
        }

        return parent::render($request, $exception);
    }
}
