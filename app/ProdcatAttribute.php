<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProdcatAttribute extends Model
{
    protected $hidden = [
        'updated_at', 'created_at', 'deleted_at'
    ];

    public function attribute(){
        return $this->belongsTo(Attribute::class,'attribute_id','id');
    }

    public function prodcat(){
        return $this->belongsTo(Prodcat::class,'prodcat_id','id');
    }
}
