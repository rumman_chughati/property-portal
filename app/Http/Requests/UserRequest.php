<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch (request()->method()){
            case 'POST':
                $rules = [
                    'email' => 'required|email|unique:users',
                    'password' => 'sometimes|min:6',
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'gender' => 'required',
                    'avatar' => 'sometimes|file|mimes:jpeg,jpg,bmp,png,svg|max:200000',
                ];
                return $rules;
                break;

            default:
                return [];
        }
    }

    public function failedValidation(Validator $validator)
    {

        $errors = $validator->errors()->getMessages();
        $errors_response = [];
        foreach ($errors as $name => $errors_array) {
            $errors_response[$name] = $errors_array[0];
        }
//    dd($errors_response);
//        return response()->json($errors_response);
//        return response()->json([
//            'status_code' => JsonResponse::HTTP_UNPROCESSABLE_ENTITY,
//            'errors' => $errors_response
//        ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
    }
}
