<?php

namespace App\Http;

use Carbon\Carbon;

trait ObjectTransformation
{

    public function userObjectTransformation($request,$expireIn)
    {
        if(empty($request)){
            return [];
        }
        $data = [
            'id' => (int)$request->id,
            'first_name' => (string)$request->first_name,
            'last_name' => (string)$request->last_name,
            'email' => (string)$request->email,
            'avatar' => (string)$request->avatar,
            'status' => (string)$request->account_status,
            'remember_token' => (string)$request->remember_token,
            'gender' => (string)$request->gender,
            'phone_number' => (string)$request->phone_number,
            'roles' => $request->roles->map(function ($role) {
                return $this->rolesObjectTransformation($role);
            }),
        ];

        if($expireIn){
            $minutes = auth()->factory()->getTTL();
            $now = Carbon::now();
            $timestamp = strtotime("+{$minutes} minutes ".$now);
            $data['expire_in'] = $timestamp;
        }

        return $data;
    }

    public function rolesObjectTransformation($role,$withPermissions = false){
        $data =  [
            'id' => $role->id,
            'name' => $role->name
        ];

        if($withPermissions) {
            $data['permissions'] = $role->permissions->map(function ($permission){
                return $this->permissionObjectTransformation($permission);
            });
        }

        return $data;
    }

    public function permissionObjectTransformation($permission){
        $data = [
            'id' => $permission->id,
            'name' => $permission->name,
        ];
        return $data;
    }

    public function propertyListingObjectTransformation($propertyListing){

        if(empty($propertyListing)){
            return [];
        }

        $data = [
            'id' => (int)$propertyListing->id,
            'sold_status' => (string)$propertyListing->sold_status,
            'approved_status'=> (string)$propertyListing->approved_status,
            'title'=> (string)$propertyListing->title,
            'amount'=> (string)$propertyListing->amount,
            'approved_by' => (int)$propertyListing->approved_by,
            'land_area'=> (string)$propertyListing->land_area,
            'approved_date'=> (string)$propertyListing->approved_date,
            'rejected_date'=> (string)$propertyListing->rejected_date,
            'ownership'=> (string)$propertyListing->ownership,
            'bidding_type'=> (string)$propertyListing->bidding_type,
            'property_classification'=> (string)$propertyListing->property_classification,
            'property_purpose' => (string)$propertyListing->property_purpose,
            'auction_start_date' => (string)$propertyListing->auction_start_date,
            'auction_end_date' => (string)$propertyListing->auction_end_date,
            'auction_total_days' => (string)$propertyListing->auction_total_days,
            'no_of_bathrooms' => (string)$propertyListing->no_of_bathrooms,
            'no_of_bedrooms' => (string)$propertyListing->no_of_bedrooms,
            'city' => (string)$propertyListing->city,
            'location' => (string)$propertyListing->location,
            'belongs_to_user' => $this->userObjectTransformation($propertyListing->user,false),
            'apporved_by_user' => $this->userObjectTransformation($propertyListing->apporvedByUser,false),
        ];

        return $data;
    }

    public function propertyFeaturesObjectTransformation($propertyFeatures,$with_sub_features=false){
        if(empty($propertyFeatures)){
            return [];
        }

        $data=[
            'id' => (int)$propertyFeatures->id,
            'name' => (string)$propertyFeatures->name,
        ];

        if($with_sub_features){
            $data['sub_features'] = $propertyFeatures->propertySubfeatures->map(function ($sub_features){
                return $this->propertySubfeaturesObjectTransformation($sub_features);
            });
        }

        return $data;
    }

    public function propertySubfeaturesObjectTransformation($propertySubFeatures){
        if(empty($propertySubFeatures)){
            return [];
        }
        $data = [
            'id' => (int)$propertySubFeatures->id,
            'name' => (string)$propertySubFeatures->name,
            'value_type' => (string)$propertySubFeatures->value_type,
            'value' => unserialize($propertySubFeatures->value)
        ];

        return $data;
    }

    public function propertyTypesObjectTransformation($propertyTypes,$with_sub_types=false){
        if(empty($propertyTypes)){
            return [];
        }

        $data=[
            'id' => (int)$propertyTypes->id,
            'name' => (string)$propertyTypes->name,
            'public' => (bool)$propertyTypes->public,
        ];

        if($with_sub_types){
            $data['sub_types'] = $propertyTypes->subTypes->map(function ($sub_types){
                return $this->propertySubTypesObjectTransformation($sub_types);
            });
        }

        return $data;
    }

    public function propertySubTypesObjectTransformation($sub_types){
        if(empty($sub_types)){
            return [];
        }
        $data = [
            'id' => (int)$sub_types->id,
            'parent_id' => (int)$sub_types->parent_id,
            'name' => (string)$sub_types->name,
            'public' => (boolean)$sub_types->public,
        ];

        return $data;
    }

    public function countryObjectTransformation($country,$with_states=false,$with_cities=false, $with_regions=false){
        if(empty($country)){
            return [];
        }

        $data=[
            'id' => (int)$country->id,
            'country_code' => (string)$country->country_code,
            'name' => (string)$country->name,
            'country_idd' => (int)$country->country_idd,
            'country_iso3' => (string)$country->country_iso3,
            'region_id' => (int)$country->region_id,
            'public' => (bool)$country->public,
        ];

        if($with_regions){
            $data['region'] = $this->regionObjectTransformation($country->region);
        }

        if($with_states){

            if($with_cities){
                $data['states'] = $country->states->map(function ($states){
                    return $this->stateObjectTransformation($states, true);
                });
            } else {
                $data['states'] = $country->states->map(function ($states) {
                    return $this->stateObjectTransformation($states, false);
                });
            }
        }

        return $data;
    }

    public function regionObjectTransformation($region,$with_country=false){
        if(empty($region)){
            return [];
        }
        $data = [
            'id' => (int)$region->id,
            'region_cd' => (string)$region->region_cd,
            'name' => (string)$region->name,
            'public' => (bool)$region->public,
        ];

        if($with_country){
            $data['countries'] = $region->countries->map(function ($country){
                return $this->countryObjectTransformation($country,false,false,false);
            });

        }

        return $data;
    }

    public function stateObjectTransformation($states, $with_cities, $with_country = false){
        if(empty($states)){
            return [];
        }
        $data = [
            'id' => (int)$states->id,
            'country_id' => (int)$states->country_id,
            'name' => (string)$states->name,
            'public' => (bool)$states->public,

        ];

        if($with_country){
            $data['country'] = $this->countryObjectTransformation($states->country,false,false,false);
        }

        if($with_cities){
            $data['cities'] = $states->cities->map(function ($cities) {
                return $this->cityObjectTransformation($cities, false);
            });
        }

        return $data;
    }

    public function cityObjectTransformation($city){
        if(empty($city)){
            return [];
        }
        $data = [
            'id' => (int)$city->id,
            'state_id' => (int)$city->state_id,
            'name' => (string)$city->name,
            'telephone_code' => (string)$city->telephone_code,
            'public' => (bool)$city->public,

        ];

        return $data;
    }

    public function organizationObjectTransformation($organization,$with_type=false,$with_location=false){
        if(empty($organization)){
            return [];
        }

        $data=[
            'id' => (int)$organization->id,
            'name' => (string)$organization->name,
            'organization_type_id' => (int)$organization->organization_type_id,
            'registration_id' => (string)$organization->registration_id,
            'uan' => (string)$organization->uan,
            'ntn' => (string)$organization->ntn,
            'stn' => (string)$organization->stn,
            'verified' => (bool)$organization->verified,

        ];

        if($with_type){
            $data['type'] = $this->typeObjectTransformation($organization->type);
        }

        if($with_location){
            $data['organization_locations'] = $organization->locations->map(function ($location) {
                return $this->locationObjectTransformation($location, true, false);
            });
        }

        return $data;
    }

    public function typeObjectTransformation($type){
        if(empty($type)){
            return [];
        }
        $data = [
            'id' => (int)$type->id,
            'name' => (string)$type->name,
            'public' => (bool)$type->public,

        ];

        return $data;
    }

    public function locationObjectTransformation($location,$with_city=false,$with_contact_person=false){
        if(empty($location)){
            return [];
        }

        $data = [
            'id' => (int)$location->id,
            'name' => (string)$location->name,
            'organization_id' => (int)$location->organization_id,
            'city_id' => (int)$location->city_id,
            'organization_location_uan' => (string)$location->organization_location_uan,
            'organization_telephone_1' => (string)$location->organization_telephone_1,
            'organization_telephone_2' => (string)$location->organization_telephone_2,
            'organization_telephone_3' => (string)$location->organization_telephone_3,
            'organization_fax_1' => (string)$location->organization_fax_1,
            'organization_fax_2' => (string)$location->organization_fax_2,
            'email' => (string)$location->email,
            'address' => (string)$location->address,
            'building' => (string)$location->building,
            'street' => (string)$location->street,
            'type' => (string)$location->type,
            'primary' => (bool)$location->primary,
            'verified' => (bool)$location->verified,

        ];

        if($with_city){
            $data['city'] = $this->cityObjectTransformation($location->city);
        }

        return $data;
    }

}

