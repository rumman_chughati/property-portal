<?php

namespace App\Http\Resources;

use App\Http\ObjectTransformation;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PropertyTypesCollection extends ResourceCollection
{
    use ObjectTransformation;

    protected $status_code = JsonResponse::HTTP_OK;
    protected $message = '';
    protected $sub_types = false;

    public function __construct($resource, $status_code = JsonResponse::HTTP_OK, string $message = '', $sub_types = false)
    {
        parent::__construct($resource);
        $this->status_code = $status_code;
        $this->message = $message;
        $this->sub_types = $sub_types;
    }

    public function toArray($request)
    {
        return [
            'status_code' => $this->status_code,
            'message' => $this->message,
            'data' => $this->collection->transform(function ($item){
                return $this->propertyTypesObjectTransformation($item,$this->sub_types);
            }),
        ];
    }
}
