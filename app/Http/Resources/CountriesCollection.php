<?php

namespace App\Http\Resources;

use App\Http\ObjectTransformation;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\JsonResponse;

class CountriesCollection extends ResourceCollection
{
    use ObjectTransformation;

    protected $status_code = JsonResponse::HTTP_OK;
    protected $message = '';
    protected $with_states = false;
    protected $with_cities = false;
    protected $with_regions = false;

    public function __construct($resource, $status_code = JsonResponse::HTTP_OK, $message = '', $with_states = false, $with_cities = false, $with_regions = false)
    {
        parent::__construct($resource);
        $this->status_code = $status_code;
        $this->message = $message;
        $this->with_states = $with_states;
        $this->with_cities = $with_cities;
        $this->with_regions = $with_regions;
    }


    public function toArray($request)
    {
        return [
            'status_code' => $this->status_code,
            'message' => $this->message,
            'data' => $this->collection->transform(function ($object){
                return $this->countryObjectTransformation($object, $this->with_states, $this->with_cities, $this->with_regions);
            }),
        ];
    }
}
