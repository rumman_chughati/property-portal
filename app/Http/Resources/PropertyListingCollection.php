<?php

namespace App\Http\Resources;

use App\Http\ObjectTransformation;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\JsonResponse;

class PropertyListingCollection extends ResourceCollection
{
    use ObjectTransformation;
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    protected $staus_code = JsonResponse::HTTP_OK;
    protected $message = '';

    public function __construct($resource, $staus_code = JsonResponse::HTTP_OK, string $message = '')
    {
        parent::__construct($resource);
        $this->staus_code = $staus_code;
        $this->message = $message;
    }


    public function toArray($request)
    {
//        return parent::toArray($request);

        return [
            'status_code' => $this->staus_code,
            'message' => $this->message,
            'data' => $this->collection->transform(function ($item){
                return $this->propertyListingObjectTransformation($item);
            }),
        ];
    }
}
