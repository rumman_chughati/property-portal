<?php

namespace App\Http\Resources;

use App\Http\ObjectTransformation;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;

class UsersResource extends JsonResource
{
    use ObjectTransformation;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    protected $staus_code = JsonResponse::HTTP_OK;
    protected $message = '';
    protected $expireIn = false;

    public function __construct($resource, $staus_code = JsonResponse::HTTP_OK, $message = '',$expireIn=false)
    {
        parent::__construct($resource);
        $this->staus_code = $staus_code;
        $this->message = $message;
        $this->expireIn = $expireIn;
    }

    public function toArray($request)
    {
//        return parent::toArray($request);
        return $this->userObjectTransformation($this,$this->expireIn);
    }

    public function with($request)
    {
        return [
            'status_code' => $this->staus_code,
            'message'=> $this->message
        ];
    }
}
