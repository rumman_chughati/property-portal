<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Features extends JsonResource
{
    use ObjectTransformation;

    protected $status_code = JsonResponse::HTTP_OK;
    protected $message = '';

    public function __construct($resource, $status_code = JsonResponse::HTTP_OK, $message = '')
    {
        parent::__construct($resource);

        $this->status_code = $status_code;
        $this->message = $message;
    }

    public function toArray($request)
    {
        return $this->featuresObjectTransformation($this);
    }

    public function with($request)
    {
        return [
            'status_code' => $this->status_code,
            'message'=> $this->message
        ];
    }
}
