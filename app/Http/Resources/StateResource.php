<?php

namespace App\Http\Resources;

use App\Http\ObjectTransformation;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;

class StateResource extends JsonResource
{
    use ObjectTransformation;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    protected $status_code = JsonResponse::HTTP_OK;
    protected $message = '';
    protected $with_country = false;
    protected $with_cities = false;

    public function __construct($resource, $status_code = JsonResponse::HTTP_OK, $message = '', $with_country = false, $with_cities = false)
    {
        parent::__construct($resource);

        $this->status_code = $status_code;
        $this->message = $message;
        $this->with_country = $with_country;
        $this->with_cities = $with_cities;
    }

    public function toArray($request)
    {
//        return parent::toArray($request);
        return $this->stateObjectTransformation($this,$this->with_cities,$this->with_country);
    }

    public function with($request)
    {
        return [
            'status_code' => $this->status_code,
            'message'=> $this->message
        ];
    }
}
