<?php

namespace App\Http\Resources;

use App\Http\ObjectTransformation;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\JsonResponse;

class PropertyListingResource extends JsonResource
{
    use ObjectTransformation;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    protected $staus_code = JsonResponse::HTTP_OK;
    protected $message = '';

    public function __construct($resource, $staus_code = JsonResponse::HTTP_OK, $message = '')
    {
        parent::__construct($resource);

        $this->staus_code = $staus_code;
        $this->message = $message;
    }

    public function toArray($request)
    {
//        return parent::toArray($request);
//        dd($this);
        return $this->propertyListingObjectTransformation($this);
    }

    public function with($request)
    {
        return [
            'status_code' => $this->staus_code,
            'message'=> $this->message
        ];
    }
}
