<?php

namespace App\Http\Resources;

use App\Http\ObjectTransformation;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\ResourceCollection;

class OrganizationsCollection extends ResourceCollection
{
    use ObjectTransformation;

    protected $status_code = JsonResponse::HTTP_OK;
    protected $message = '';
    protected $with_type = false;
    protected $with_location = false;

    public function __construct($resource, $status_code = JsonResponse::HTTP_OK, $message = '', $with_type = false, $with_location = false)
    {
        parent::__construct($resource);

        $this->status_code = $status_code;
        $this->message = $message;
        $this->with_type = $with_type;
        $this->with_location = $with_location;
    }

    public function toArray($request)
    {
        return [
            'status_code' => $this->status_code,
            'message' => $this->message,
            'data' => $this->collection->transform(function ($object){
                return $this->organizationObjectTransformation($object, $this->with_type, $this->with_location);
            }),
        ];
    }

}
