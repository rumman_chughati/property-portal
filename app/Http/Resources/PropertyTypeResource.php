<?php

namespace App\Http\Resources;

use App\Http\ObjectTransformation;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;

class PropertyTypeResource extends JsonResource
{
    use ObjectTransformation;

    protected $status_code = JsonResponse::HTTP_OK;
    protected $message = '';
    protected $sub_type = false;

    public function __construct($resource, $status_code = JsonResponse::HTTP_OK, $message = '', $sub_type = false)
    {
        parent::__construct($resource);
        $this->status_code = $status_code;
        $this->message = $message;
        $this->sub_type = $sub_type;
    }

    public function toArray($request)
    {
        return $this->propertyTypesObjectTransformation($this,$this->sub_type);
    }

    public function with($request)
    {
        return [
            'status_code' => $this->status_code,
            'message'=> $this->message
        ];
    }
}
