<?php

namespace App\Http\Resources;

use App\Http\ObjectTransformation;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;


class RolesResource extends JsonResource
{
    use ObjectTransformation;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected $staus_code = JsonResponse::HTTP_OK;
    protected $message = '';
    protected $withPermissions = false;

    public function __construct($resource, $staus_code = JsonResponse::HTTP_OK, $message = '',bool $withPermissions)
    {
        parent::__construct($resource);
        $this->staus_code = $staus_code;
        $this->message = $message;
        $this->withPermissions = $withPermissions;
    }

    public function toArray($request)
    {
//        return parent::toArray($request);
        return $this->rolesObjectTransformation($this,$this->withPermissions);
    }

    public function with($request)
    {
        return[
            'status_code' => $this->staus_code,
            'message'=> $this->message
        ];
    }
}
