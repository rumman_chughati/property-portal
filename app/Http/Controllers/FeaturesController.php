<?php

namespace App\Http\Controllers;

use App\Http\ObjectTransformation;
use App\Http\Resources\PropertyFeatureResource;
use App\Http\Resources\PropertyFeaturesCollection;
use App\Feature;
use App\PropertyType;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Validator;

class FeaturesController extends Controller
{
    use ObjectTransformation;

    /*
     *  Property Features
     */
    public function deleteFeatureById($id){
        try{

            $feature = Feature::where('id', $id)->where('parent_id', Null)->first();
            if(!empty($feature)){

                //sub-features deletion
                $subFeatures = Feature::where('parent_id', $id)->get()->pluck('id');
                Feature::destroy($subFeatures);

                //feature deletion
                $feature->delete($id);

                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->jsonResponseWithMessage("Feature Deleted: ".$feature->name);

            } else {
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Provided Feature does not Exists!");
            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function getAllPropertyFeatures(){
        try{
            $count = Feature::all()->count();
            if($count > 0){
                $features = Feature::with('propertySubfeatures')->where('parent_id',null)->get();
                return (new PropertyFeaturesCollection($features,JsonResponse::HTTP_OK,"Features Found",true))
                    ->response()->setStatusCode(JsonResponse::HTTP_OK);
            }else{
                $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                return $this->jsonResponseWithErrors("Property Features Not Found");
            }

        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function addNewFeature(Request $request){
        try{

            $validator = Validator::make($request->all(), [
                'property_type_id' => 'required',
                'name' => 'required|unique:features'
            ]);

            if($validator->fails()) {

                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);

            }else{

                $propertyType = PropertyType::where('id',$request->input('property_type_id'))->where('parent_id', '<>', Null)->first();
                if($propertyType != '') {

                    $features = new Feature;
                    $features->name = $request->input('name');
                    $features->property_type_id = $request->input('property_type_id');
                    $features->created_at = Carbon::now();
                    $features->updated_at = Carbon::now();

                    $features->save();

                    return (new PropertyFeatureResource($features, JsonResponse::HTTP_OK, "Feature Added!", true))
                        ->response()->setStatusCode(JsonResponse::HTTP_OK);

                } else {
                    $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                    return $this->jsonResponseWithErrors("The Provided Property Type does not Exists!");
                }

            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function updateFeatureById(Request $request){
        try{

            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'name' => 'required|unique:features'
            ]);

            if($validator->fails()) {

                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);

            }else{

                $feature = Feature::where('id', $request->input('id'))->where('parent_id', Null)->first();
                if(!empty($feature)){

                    //feature update
                    $feature->name = $request->input('name');
                    $feature->updated_at = Carbon::now();
                    $feature->save();

                    $this->setStatusCode(JsonResponse::HTTP_OK);
                    return $this->jsonResponseWithMessage("Feature Updated: ".$feature->name);

                } else {
                    $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                    return $this->jsonResponseWithErrors("The Provided Feature does not Exists!");
                }
            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function getPropertyFeatureById($id){
        try{
            if($id != ''){
                $count = Feature::where('id',$id)->count();
                if($count > 0){
                    $feature = Feature::where('id',$id)->with('propertySubfeatures')->first();
                    return (new PropertyFeatureResource($feature,JsonResponse::HTTP_OK,"Feature Found",true))
                        ->response()->setStatusCode(JsonResponse::HTTP_OK);
                }else{
                    $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                    return $this->jsonResponseWithErrors("Property Feature Not Found");
                }
            }else{
                $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                return $this->jsonResponseWithErrors("Property Features Id Not Provided");
            }
        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    /*
     *  Property Sub-Features
     */
    public function deleteSubFeatureById($id){
        try{
            $subFeature = Feature::where('id',$id)->where('parent_id', '<>', Null)->first();
            if(!empty($subFeature)){
                $subFeature->delete($subFeature->id);
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->jsonResponseWithMessage("Sub-Feature Deleted: ".$subFeature->name);
            } else {
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Provided Sub-Feature does not Exists!");
            }
        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function addNewSubFeature(Request $request){
        try{

            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'property_type_id' => 'required',
                'name' => 'required|unique:features',
                'value_type' => 'required',
                'value' => 'required|array'
            ]);

            if($validator->fails()) {

                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);

            }else{

                $propertyType = PropertyType::where('id',$request->input('property_type_id'))->where('parent_id', '<>', Null)->first();
                if($propertyType != '') {

                    $feature = Feature::where('id', $request->input('id'))->where('parent_id',null)->first();
                    if(!empty($feature)){

                        $subFeature = new Feature;

                        $subFeature->name = $request->input('name');
                        $subFeature->parent_id = $request->input('id');
                        if( !empty( $request->input('value') ) ){
                            $subFeature->value = $request->input('value');
                        }
                        $subFeature->property_type_id = $request->input('property_type_id');
                        $subFeature->value_type = $request->input('value_type');
                        $subFeature->value = serialize($request->input('value'));
                        $subFeature->created_at = Carbon::now();
                        $subFeature->updated_at = Carbon::now();

                        $subFeature->save();
                        $this->setStatusCode(JsonResponse::HTTP_OK);
                        return $this->customReposneWithStatusAndMessage($subFeature,'Sub-Feature Added!');

                    } else {
                        $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                        return $this->jsonResponseWithErrors("The Provided Feature does not Exists!");
                    }

                } else {
                    $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                    return $this->jsonResponseWithErrors("The Provided Property Type does not Exists!");
                }
            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function updateSubFeatureById(Request $request){
        try{

            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'name' => 'unique:features',
                'value' => 'string'
            ]);

            $value = $request->input('value');

            if($validator->fails()) {

                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);

            }else{

                $subFeature = Feature::where('id', $request->input('id'))->where('parent_id', '<>', Null)->first();
                if(!empty($subFeature)){

                    if(!empty($request->input('name'))){
                        $subFeature->name = $request->input('name');
                    }
                    if($value != ''){
                        $existing_values = unserialize($subFeature->value);
                        if(!in_array($value, $existing_values)){
                            //array_push($existing_values, $value);
                        } else {
                            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                            return $this->jsonResponseWithErrors("The Provided Value Already Exists in the Sub-Feature!");
                        }
                    }

                    exit;
                    $subFeature->updated_at = Carbon::now();
                    $subFeature->save();

                    $this->setStatusCode(JsonResponse::HTTP_OK);
                    return $this->customReposneWithStatusAndMessage($subFeature,'Sub-Feature Updated!');

                } else {
                    $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                    return $this->jsonResponseWithErrors("The Provided Sub-Feature does not Exists!");
                }
            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

}
