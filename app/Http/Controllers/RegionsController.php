<?php

namespace App\Http\Controllers;

use App\Block;
use App\City;
use App\Http\ObjectTransformation;
use App\Http\Resources\CountriesCollection;
use App\Http\Resources\CountryResource;
use App\Http\Resources\RegionsCollection;
use App\Http\Resources\RegionsResource;
use App\Http\Resources\StateResource;
use App\Http\Resources\StatesCollection;
use App\Phase;
use App\Region;
use App\Scheme;
use App\State;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Country;
use Illuminate\Validation\Rule;
use PHPUnit\Framework\Constraint\Count;
use Psy\Shell;
use Validator;

class RegionsController extends Controller
{
    use ObjectTransformation;

    /*
     *  Regions
     */

    public function deleteRegionById($id){
        try{

            $region = Region::where('id', $id)->first();
            if(!empty($region)){

                $region->delete($id);

                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->jsonResponseWithMessage("Country Deleted: ".$region->name);

            } else {
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Provided Region does not Exists!");
            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function getAllRegions(){
        try{
            $regions = Region::with('countries.states')->get();
            return (new RegionsCollection($regions,JsonResponse::HTTP_OK,"Regions Found!", true))
                ->response()->setStatusCode(JsonResponse::HTTP_OK);
        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function addNewRegion(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'region_cd' => 'required|unique:regions|regex:/^[0-9]*$/u|min:1|max:5',
                'name' => 'required|unique:regions|max:100',
                'public' => 'required|boolean'
            ]);
            if($validator->fails()){
                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);
            }else{
                $region = new Region;
                $region->region_cd = $request->input('region_cd');
                $region->name = $request->input('name');
                $region->public = $request->input('public');
                $region->created_at = Carbon::now();
                $region->updated_at = Carbon::now();
                $region->save();

                return (new RegionsResource($region,JsonResponse::HTTP_OK,"Region Added!", false))
                    ->response()->setStatusCode(JsonResponse::HTTP_OK);
            }
        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function updateRegionById(Request $request){
        try{

            if( empty($request->input('id')) ){
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Region ID must be provided!");
            }
            $validator = Validator::make($request->all(), [
                'region_cd' => ["min:1",
                    "max:5",
                    "regex:/^[0-9]*$/u",
                    Rule::unique('regions')->ignore($request->input('id'))],
                'name' => ["required","max:100",
                    Rule::unique('regions')->ignore($request->input('id'))],
                'public' => 'required|boolean'
            ]);
            if($validator->fails()) {
                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);
            }else{
                $region_id = $request->input('id');
                $region = Region::where('id', $region_id)->first();
                if(!empty($region)){
                    if( !empty($request->input('region_cd')) ) {
                        $region->region_cd = $request->input('region_cd');
                    }
                    if( !empty($request->input('name')) ) {
                        $region->name = $request->input('name');
                    }

                    $region->public = $request->input('public');


                    $region->updated_at = Carbon::now();
                    $region->save();

                    return (new RegionsResource($region,JsonResponse::HTTP_OK,"Region Updated!", false))
                        ->response()->setStatusCode(JsonResponse::HTTP_OK);
                }else {
                    $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                    return $this->jsonResponseWithErrors("The Provided Region does not Exists!");
                }
            }
        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function getRegionById($id){
        try{
            if($id != '') {
                $region = Region::where('id',$id)->with('countries')->first();
                if( !empty($region) ){
                    return (new RegionsResource($region,JsonResponse::HTTP_OK,"Region Found", false))
                        ->response()->setStatusCode(JsonResponse::HTTP_OK);
                } else {
                    $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                    return $this->jsonResponseWithErrors("The Provided Region does not Exists!");
                }

            } else {
                $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                return $this->jsonResponseWithErrors("State ID must be provided!");
            }
        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    /*
     *  Countries
     */
    public function deleteCountryById($id){
        try{

            $country = Country::where('id', $id)->first();
            if(!empty($country)){

                $country->delete($id);

                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->jsonResponseWithMessage("Country Deleted: ".$country->name);

            } else {
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Provided Country does not Exists!");
            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function getAllCountries(){
        try{
            $countries = Country::with('states','region')->get();
            return (new CountriesCollection($countries,JsonResponse::HTTP_OK,"Countries Found!", true, true, true))
                ->response()->setStatusCode(JsonResponse::HTTP_OK);
        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function addNewCountry(Request $request){
        try{

            $validator = Validator::make($request->all(), [
                'country_code' => 'required|unique:countries|regex:/^[a-zA-Z]+$/u|min:2|max:2',
                'name' => 'required|max:100',
                'country_idd' => 'required|unique:countries|digits_between:1,10',
                'country_iso3' => 'required|unique:countries|regex:/^[a-zA-Z]+$/u|min:3|max:3',
                'public' => 'required|boolean',
                'region_id' => 'required',
            ]);

            if($validator->fails()) {

                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);

            }else{

                $region_id = $request->input('region_id');
                $country_name = $request->input('name');

                $region = Region::where('id', $region_id)->first();
                if(!empty($region)){
                    $this_country = Country::where('region_id', $region_id)->get()->pluck('name')->toArray();
                    if(!in_array($country_name, $this_country)){
                        $country = new Country;
                        $country->country_code = strtoupper($request->input('country_code'));
                        $country->name = $request->input('name');
                        $country->country_idd = $request->input('country_idd');
                        $country->country_iso3 = strtoupper($request->input('country_iso3'));
                        $country->public = $request->input('public');
                        $country->region_id = $request->input('region_id');
                        $country->created_at = Carbon::now();
                        $country->updated_at = Carbon::now();

                        $country->save();
                        $country = Country::with('region')->where('id',$country->id)->first();

                        return (new CountryResource($country, JsonResponse::HTTP_OK, "Country Added!", false, false, true))
                            ->response()->setStatusCode(JsonResponse::HTTP_OK);
                    }else {
                        $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                        return $this->jsonResponseWithErrors("The Country Already Exists!");
                    }
                }else{
                    $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                    return $this->jsonResponseWithErrors("The Provided Region does not Exists!");
                }
            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function updateCountryById(Request $request){
        try{

            if( empty($request->input('id')) ){
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Country ID must be provided!");
            }
            $validator = Validator::make($request->all(), [
                'country_code' => ["min:2",
                    "max:2",
                    "regex:/^[a-zA-Z]+$/u",
                    Rule::unique('countries')->ignore($request->input('id'))],
                'name' => ["max:100",
                    Rule::unique('countries')->ignore($request->input('id'))],
                'country_idd' => ["digits_between:1,10",
                    Rule::unique('countries')->ignore($request->input('id'))],
                'country_iso3' => ["min:3",
                    "max:3",
                    "regex:/^[a-zA-Z]+$/u",
                    Rule::unique('countries')->ignore($request->input('id'))],
                'public' => 'required|boolean',
                'region_id' => "required",

            ]);

            $country_id = $request->input('id');

            if($validator->fails()) {

                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);

            }else{
                $country_name = $request->input('name');
                $country_id = $request->input('id');
                $country = Country::where('id', $country_id)->with('region')->first();
                if(!empty($country)){

                    if( !empty($country_name) ) {
                        $region_countries = Country::where('region_id', $country->region_id)
                            ->where('id', '<>', $country_id)
                            ->get()
                            ->pluck('name')
                            ->toArray();

                        if( !in_array($country_name, $region_countries) ){
                            $country->name = $country_name;
                        } else {
                            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                            return $this->jsonResponseWithErrors("The Provided Country Name Already Exists!");
                        }
                    }
                    //country update
//                    if( !empty($request->input('name')) ) {
//                        $country->name = $request->input('name');
//                    }
                    if( !empty($request->input('country_code')) ) {
                        $country->country_code = strtoupper($request->input('country_code'));
                    }
                    if( !empty($request->input('country_idd')) ) {
                        $country->country_idd = $request->input('country_idd');
                    }
                    if( !empty($request->input('country_iso3')) ) {
                        $country->country_iso3 = strtoupper($request->input('country_iso3'));
                    }
                    if( !empty($request->input('region_id')) ) {
                        $country->region_id = $request->input('region_id');
                    }

                    $country->public = $request->input('public');

                    $country->updated_at = Carbon::now();
                    $country->save();
                    $country = Country::where('id', $country->id)->with('region')->first();
                    $this->setStatusCode(JsonResponse::HTTP_OK);
                    return $this->customReposneWithStatusAndMessage($country, 'Country Updated!');

                } else {
                    $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                    return $this->jsonResponseWithErrors("The Provided Country does not Exists!");
                }
            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function getCountryById($id){
        try{
            if($id != '') {
                $country = Country::where('id',$id)->with('states')->first();
                return (new CountryResource($country, JsonResponse::HTTP_OK, "Country Found!", true, true, true))
                    ->response()->setStatusCode(JsonResponse::HTTP_OK);
            } else {
                $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                return $this->jsonResponseWithErrors("Country ID must be provided!");
            }
        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    /*
     *  States
     */
    public function deleteStateById($id){
        try{

            $state = State::where('id', $id)->first();
            if(!empty($state)){

                $state->delete($id);

                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->jsonResponseWithMessage("State Deleted: ".$state->name);

            } else {
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Provided State does not Exists!");
            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function getAllStates(){
        try{
            $states = State::with('country','cities')->get();
            return (new StatesCollection($states,JsonResponse::HTTP_OK,"States Found!", true, true))
                ->response()->setStatusCode(JsonResponse::HTTP_OK);

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function addNewState(Request $request){
        try{

            $validator = Validator::make($request->all(), [
                'country_id' => 'required',
                'name' => 'required|max:100',
                'public' => 'required|boolean'
            ]);

            $country_id = $request->input('country_id');
            $state_name = $request->input('name');

            if($validator->fails()) {

                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);

            }else{

                $country = Country::where('id', $country_id)->first();
                if( !empty($country) ) {

                    $this_states = State::where('country_id', $country_id)->get()->pluck('name')->toArray();
                    if( !in_array($state_name, $this_states) ) {
                        $state = new State;
                        $state->country_id = $country_id;
                        $state->name = $state_name;
                        $state->public = $request->input('public');

                        $state->created_at = Carbon::now();
                        $state->updated_at = Carbon::now();

                        $state->save();

                        $state = State::where('id',$state->id)->with('country')->first();

                        return (new StateResource($state,JsonResponse::HTTP_OK,"States Added!", true, false))
                            ->response()->setStatusCode(JsonResponse::HTTP_OK);
                    } else {
                        $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                        return $this->jsonResponseWithErrors("The Provided State Already Exists!");
                    }

                } else {
                    $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                    return $this->jsonResponseWithErrors("The Provided Country does not Exists!");
                }

            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function updateStateById(Request $request){
        try{

            if(empty($request->input('id')) ){
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The State ID must be provided!");
            }
            $validator = Validator::make($request->all(), [
                'name' => 'max:100',
                'public' => 'required|boolean'
            ]);

            $state_id = $request->input('id');

            if($validator->fails()) {

                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);

            }else{

                $state = State::where('id', $state_id)->with('country')->first();
                if(!empty($state)){

                    //state update
                    if( !empty($request->input('name')) ) {
                        $state_name = $request->input('name');

                        $country_states = State::where('country_id', $state->country_id)
                            ->where('id', '<>', $state_id)
                            ->get()
                            ->pluck('name')
                            ->toArray();

                        if( !in_array($state_name, $country_states) ){
                            $state->name = $state_name;
                        } else {
                            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                            return $this->jsonResponseWithErrors("The Provided State Name Already Exists!");
                        }
                    }

                    $state->public = $request->input('public');

                    $state->updated_at = Carbon::now();
                    $state->save();

                    $new_state = State::where('id', $state->id)->with('country')->first();

                    return (new StateResource($new_state,JsonResponse::HTTP_OK,"States Updated!", true, false))
                        ->response()->setStatusCode(JsonResponse::HTTP_OK);

                } else {
                    $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                    return $this->jsonResponseWithErrors("The Provided State does not Exists!");
                }
            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function getStateById($id){
        try{
            if($id != '') {
                $states = State::where('id',$id)->with('cities')->first();
                return (new StateResource($states,JsonResponse::HTTP_OK,"States Found", true, false))
                    ->response()->setStatusCode(JsonResponse::HTTP_OK);
            } else {
                $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                return $this->jsonResponseWithErrors("State ID must be provided!");
            }
        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    /*
     *  Cities
     */
    public function deleteCityById($id){
        try{

            $city = City::where('id', $id)->first();
            if(!empty($city)){

                $city->delete($id);

                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->jsonResponseWithMessage("City Deleted: ".$city->name);

            } else {
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Provided City does not Exists!");
            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function getAllCities(){
        try{

            $cities = City::with('state')->get();

            $this->setStatusCode(JsonResponse::HTTP_OK);
            return $this->customReposneWithStatusAndMessage($cities, 'Cities Found!');

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function addNewCity(Request $request){
        try{

            $validator = Validator::make($request->all(), [
                'state_id' => 'required',
                'name' => 'required|max:100',
                'telephone_code' => 'required|unique:cities|string|max:5',
                'public' => 'required|boolean'
            ]);

            $state_id = $request->input('state_id');
            $city_name = $request->input('name');

            if($validator->fails()) {

                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);

            }else{

                $state = State::where('id', $request->input('state_id'))->first();
                if( !empty($state) ) {
                    $this_cities = City::where('state_id', $state_id)->get()->pluck('name')->toArray();
                    if(!in_array($city_name, $this_cities) ) {
                        $city = new City;
                        $city->state_id = $state_id;
                        $city->name = $city_name;
                        $city->telephone_code = $request->input('telephone_code');
                        $city->public = $request->input('public');

                        $city->created_at = Carbon::now();
                        $city->updated_at = Carbon::now();

                        $city->save();

                        $new_city = City::where('id', $city->id)->with('state')->first();

                        $this->setStatusCode(JsonResponse::HTTP_OK);
                        return $this->customReposneWithStatusAndMessage($new_city, 'City Added!');

                    } else {
                        $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                        return $this->jsonResponseWithErrors("The Provided City Already Exists!");
                    }

                } else {
                    $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                    return $this->jsonResponseWithErrors("The Provided State does not Exists!");
                }

            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function updateCityById(Request $request){
        try{

            if( empty($request->input('id')) ){
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The City ID must be provided!");
            }
            $validator = Validator::make($request->all(), [
                'name' => 'max:100',
                'telephone_code' => 'string|max:5',
                'public' => 'required|boolean'
            ]);

            $city_id = $request->input('id');

            if($validator->fails()) {

                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);

            }else{

                $city = City::where('id', $city_id)->with('state')->first();
                if(!empty($city)){

                    //state update

                    $state_cities = City::where('state_id', $city->state_id)->where('id', '<>', $city_id)->get();

                    if( !empty($request->input('name')) ) {
                        $city_name = $request->input('name');

                        $cities_names = $state_cities->pluck('name')->toArray();

                        if( !in_array($city_name, $cities_names) ){
                            $city->name = $city_name;
                        } else {
                            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                            return $this->jsonResponseWithErrors("The Provided City Name Already Exists!");
                        }
                    }

                    if( !empty($request->input('telephone_code')) ) {
                        $city_telephone_code = $request->input('telephone_code');

                        $cities_telephone_codes = $state_cities->pluck('telephone_code')->toArray();

                        if( !in_array($city_telephone_code, $cities_telephone_codes) ){
                            $city->telephone_code = $city_telephone_code;
                        } else {
                            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                            return $this->jsonResponseWithErrors("The Provided City Telephone Code Already Exists!");
                        }
                    }

                    $city->public = $request->input('public');

                    $city->updated_at = Carbon::now();
                    $city->save();

                    $this->setStatusCode(JsonResponse::HTTP_OK);
                    return $this->customReposneWithStatusAndMessage($city, 'City Updated!');

                } else {
                    $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                    return $this->jsonResponseWithErrors("The Provided City does not Exists!");
                }
            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function getCityById($id){
        try{
            if($id != '') {
                $city = City::where('id',$id)->first();
                if( !empty($city) ){
                    $this->setStatusCode(JsonResponse::HTTP_OK);
                    return $this->customReposneWithStatusAndMessage($city, 'City Found!');
                } else {
                    $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                    return $this->jsonResponseWithErrors("The Provided City does not Exists!");
                }

            } else {
                $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                return $this->jsonResponseWithErrors("City ID must be provided!");
            }
        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    /*
     *  Schemes
     */
    public function getAllSchemes(){
        try{
            $schemes = Scheme::with('phases','city.state.country')->get();

            $this->setStatusCode(JsonResponse::HTTP_OK);
            return $this->customReposneWithStatusAndMessage($schemes, 'Schemes Found!');
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function addNewScheme(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:100',
                'public' => 'required|boolean',
                'city_id' => 'required',
            ]);

            if($validator->fails()) {
                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);
            }else{
                $city_id = $request->input('city_id');
                $scheme_name = $request->input('name');

                $city = City::where('id', $city_id)->first();
                if(!empty($city)){
                    $this_scheme = Scheme::where('city_id', $city_id)->get()->pluck('name')->toArray();
                    if(!in_array($scheme_name, $this_scheme)){
                        $scheme = new Scheme;
                        $scheme->name = $scheme_name;
                        $scheme->city_id = $city_id;
                        $scheme->public = $request->input('public');
                        $scheme->created_at = Carbon::now();
                        $scheme->updated_at = Carbon::now();

                        $scheme->save();

                        $new_scheme = Scheme::with('city.state.country','phases')->where('id',$scheme->id)->first();

                        $this->setStatusCode(JsonResponse::HTTP_OK);
                        return $this->customReposneWithStatusAndMessage($new_scheme, 'Schemes Added');
                    }else{
                        $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                        return $this->jsonResponseWithErrors("The Scheme Already Exists In This City!");
                    }
                }else{
                    $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                    return $this->jsonResponseWithErrors("The City Provided Is Not Found!");
                }
            }
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function deleteSchemeById($id){
        try{
            $scheme = Scheme::where('id', $id)->first();
            if(!empty($scheme)){
                $scheme->delete($id);
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->jsonResponseWithMessage("Scheme Deleted: ".$scheme->name);
            } else {
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Provided Scheme does not Exists!");
            }
        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function updateSchemeById(Request $request){
        try{
            if( empty($request->input('id')) ){
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Scheme ID must be provided!");
            }
            $validator = Validator::make($request->all(), [
                'name' => ["max:100",
                    Rule::unique('schemes')->ignore($request->input('id'))],
                'public' => 'required|boolean',
            ]);
            if($validator->fails()) {
                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);
            }else{
                $scheme_id = $request->input('id');
                $scheme_name = $request->input('name');

                $scheme = Scheme::where('id', $scheme_id)->with('city')->first();
                if(!empty($scheme)){
                    if(!empty($scheme_name)){
                        $city_scheme = Scheme::where('city_id', $scheme->city_id)
                            ->where('id', '<>', $scheme_id)
                            ->get()
                            ->pluck('name')
                            ->toArray();
                        if(!in_array($scheme_name, $city_scheme)){
                            $scheme->name = $scheme_name;
                        } else {
                            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                            return $this->jsonResponseWithErrors("The Provided Scheme Name Already Exists!");
                        }
                    }
                    $scheme->public = $request->input('public');
                    $scheme->updated_at = Carbon::now();

                    $scheme->save();

                    $updated_scheme = Scheme::where('id', $scheme->id)->with('city.state.country','phases')->first();
                    $this->setStatusCode(JsonResponse::HTTP_OK);
                    return $this->customReposneWithStatusAndMessage($updated_scheme, 'Scheme Updated!');
                } else {
                    $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                    return $this->jsonResponseWithErrors("The Provided Scheme does not Exists!");
                }
            }
        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function getSchemeById($id){
        try{
            if($id != '') {
                $scheme = Scheme::where('id',$id)->with('city')->first();
                if( !empty($scheme) ){
                    $this->setStatusCode(JsonResponse::HTTP_OK);
                    return $this->customReposneWithStatusAndMessage($scheme, 'Scheme Found!');
                } else {
                    $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                    return $this->jsonResponseWithErrors("The Provided Scheme does not Exists!");
                }
            } else {
                $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                return $this->jsonResponseWithErrors("Scheme ID must be provided!");
            }
        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    /*
     *  Phases
     */
    public function getAllPhases(){
        try{
            $phases = Phase::with('scheme.city','blocks')->get();
            $this->setStatusCode(JsonResponse::HTTP_OK);
            return $this->customReposneWithStatusAndMessage($phases, 'Phases Found!');
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function addNewPhase(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:100',
                'public' => 'required|boolean',
                'scheme_id' => 'required',
            ]);

            if($validator->fails()) {
                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);
            }else{
                $scheme_id = $request->input('scheme_id');
                $phase_name = $request->input('name');

                $scheme = Scheme::where('id', $scheme_id)->first();
                if(!empty($scheme)){
                    $this_phase = Phase::where('scheme_id', $scheme_id)->get()->pluck('name')->toArray();
                    if(!in_array($phase_name, $this_phase)){
                        $phase = new Phase;
                        $phase->name = $phase_name;
                        $phase->scheme_id = $scheme_id;
                        $phase->public = $request->input('public');
                        $phase->created_at = Carbon::now();
                        $phase->updated_at = Carbon::now();

                        $phase->save();

                        $new_phase = Phase::with('scheme.city','blocks')->where('id',$phase->id)->first();

                        $this->setStatusCode(JsonResponse::HTTP_OK);
                        return $this->customReposneWithStatusAndMessage($new_phase, 'Phase Added');
                    }else{
                        $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                        return $this->jsonResponseWithErrors("The Phase Already Exists In This Scheme!");
                    }
                }else{
                    $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                    return $this->jsonResponseWithErrors("The Scheme Provided Is Not Found!");
                }
            }
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function deletePhaseById($id){
        try{
            $phase = Phase::where('id', $id)->first();
            if(!empty($phase)){
                $phase->delete($id);
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->jsonResponseWithMessage("Phase Deleted: ".$phase->name);
            } else {
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Provided Phase does not Exists!");
            }
        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function updatePhaseById(Request $request){
        try{
            if( empty($request->input('id')) ){
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Phase ID must be provided!");
            }
            $validator = Validator::make($request->all(), [
                'name' => ["max:100",
                    Rule::unique('phases')->ignore($request->input('id'))],
                'public' => 'required|boolean',
                'scheme_id' => "required",
            ]);
            if($validator->fails()) {
                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);
            }else{
                $phase_id = $request->input('id');
                $phase_name = $request->input('name');

                $phase = Phase::where('id', $phase_id)->with('scheme')->first();
                if(!empty($phase)){
                    if(!empty($phase_name)){
                        $scheme_phase = Phase::where('scheme_id', $phase->scheme_id)
                            ->where('id', '<>', $phase_id)
                            ->get()
                            ->pluck('name')
                            ->toArray();
                        if(!in_array($phase_name, $scheme_phase)){
                            $phase->name = $phase_name;
                        } else {
                            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                            return $this->jsonResponseWithErrors("The Provided Phase Name Already Exists!");
                        }
                    }
                    $phase->public = $request->input('public');
                    $phase->updated_at = Carbon::now();

                    $phase->save();

                    $updated_phase = Phase::where('id', $phase->id)->with('scheme.city','blocks')->first();
                    $this->setStatusCode(JsonResponse::HTTP_OK);
                    return $this->customReposneWithStatusAndMessage($updated_phase, 'Phase Updated!');
                } else {
                    $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                    return $this->jsonResponseWithErrors("The Provided Phase does not Exists!");
                }
            }
        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function getPhaseById($id){
        try{
            if($id != '') {
                $phase = Phase::where('id',$id)->with('scheme')->first();
                if( !empty($phase)){
                    $this->setStatusCode(JsonResponse::HTTP_OK);
                    return $this->customReposneWithStatusAndMessage($phase, 'Phase Found!');
                } else {
                    $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                    return $this->jsonResponseWithErrors("The Provided Phase does not Exists!");
                }
            } else {
                $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                return $this->jsonResponseWithErrors("Phase ID must be provided!");
            }
        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    /*
     *  Blocks
     */
    public function getAllBlocks(){
        try{
            $blocks = Block::with('phase.scheme.city')->get();
            $this->setStatusCode(JsonResponse::HTTP_OK);
            return $this->customReposneWithStatusAndMessage($blocks, 'Blocks Found!');
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function addNewBlock(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:100',
                'public' => 'required|boolean',
                'phase_id' => 'required',
            ]);
            if($validator->fails()) {
                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);
            }else{
                $phase_id = $request->input('phase_id');
                $block_name = $request->input('name');

                $phase = Phase::where('id', $phase_id)->first();
                if(!empty($phase)){
                    $this_block = Block::where('phase_id', $phase_id)->get()->pluck('name')->toArray();
                    if(!in_array($block_name, $this_block)){
                        $block = new Block;
                        $block->name = $block_name;
                        $block->phase_id = $phase_id;
                        $block->public = $request->input('public');
                        $block->created_at = Carbon::now();
                        $block->updated_at = Carbon::now();

                        $block->save();

                        $new_block = Block::with('phase.scheme.city')->where('id',$block->id)->first();

                        $this->setStatusCode(JsonResponse::HTTP_OK);
                        return $this->customReposneWithStatusAndMessage($new_block, 'Block Added');
                    }else{
                        $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                        return $this->jsonResponseWithErrors("The Block Already Exists In This Scheme!");
                    }
                }else{
                    $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                    return $this->jsonResponseWithErrors("The Phase Provided Is Not Found!");
                }
            }
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function deleteBlockById($id){
        try{
            $block = Block::where('id', $id)->first();
            if(!empty($block)){
                $block->delete($id);
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->jsonResponseWithMessage("Block Deleted: ".$block->name);
            } else {
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Provided Block does not Exists!");
            }
        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function updateBlockById(Request $request){
        try{
            if( empty($request->input('id')) ){
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Block ID must be provided!");
            }
            $validator = Validator::make($request->all(), [
                'name' => ["max:100",
                    Rule::unique('blocks')->ignore($request->input('id'))],
                'public' => 'required|boolean',
                'phase_id' => "required",
            ]);
            if($validator->fails()) {
                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);
            }else{
                $block_id = $request->input('id');
                $block_name = $request->input('name');

                $block = Block::where('id', $block_id)->with('phase')->first();
                if(!empty($block)){
                    if(!empty($block_name)){
                        $phase_block = Block::where('phase_id', $block->phase_id)
                            ->where('id', '<>', $block_id)
                            ->get()
                            ->pluck('name')
                            ->toArray();
                        if(!in_array($block_name, $phase_block)){
                            $block->name = $block_name;
                        } else {
                            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                            return $this->jsonResponseWithErrors("The Provided Block Name Already Exists!");
                        }
                    }
                    $block->public = $request->input('public');
                    $block->updated_at = Carbon::now();

                    $block->save();

                    $updated_block = Block::where('id', $block->id)->with('phase.scheme.city')->first();
                    $this->setStatusCode(JsonResponse::HTTP_OK);
                    return $this->customReposneWithStatusAndMessage($updated_block, 'Block Updated!');
                } else {
                    $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                    return $this->jsonResponseWithErrors("The Provided Block does not Exists!");
                }
            }
        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function getBlockById($id){
        try{
            if($id != '') {
                $block = Block::where('id',$id)->with('phase')->first();
                if( !empty($block)){
                    $this->setStatusCode(JsonResponse::HTTP_OK);
                    return $this->customReposneWithStatusAndMessage($block, 'Block Found!');
                } else {
                    $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                    return $this->jsonResponseWithErrors("The Provided Block does not Exists!");
                }
            } else {
                $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                return $this->jsonResponseWithErrors("Block ID must be provided!");
            }
        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

}
