<?php

namespace App\Http\Controllers;

use App\Portal;
use App\Uom;
use App\UomConversion;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Validation\Rule;


class PortalsController extends Controller
{

    //get all protals
    public function getAllPortals(){
        try{
            $portals = Portal::with('uom')->get();
            $this->setStatusCode(JsonResponse::HTTP_OK);
            return $this->customReposneWithStatusAndMessage($portals, 'Portals');
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    //delete a specific portal
    public function deletePortal($id){
        try{
            $portal = Portal::where('id', $id)->first();
            if(!empty($portal)){
                $portal->delete($id);
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->jsonResponseWithMessage("Portal Deleted: ".$portal->name);
            }else{
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Provided Portal does not Exists!");
            }
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    //add new portal
    public function addNewPortal(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:50|unique:portals,name',
                'status' => 'required',
                'public' => 'required|boolean'
            ]);

            if($validator->fails()) {
                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);
            }else{
                $portal = new Portal;
                $portal->name = $request->input('name');
                $portal->status = $request->input('status');
                $portal->public = $request->input('public');
                $portal->created_at = Carbon::now();
                $portal->updated_at = Carbon::now();
                $portal->save();

                $new_portal = Portal::where('id',$portal->id)->first();
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->customReposneWithStatusAndMessage($new_portal, 'Portal Added');
            }
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    //update a specific portal
    public function updatePortal(Request $request){
        try{
            if(empty($request->input('id')) ){
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Portal ID must be provided!");
            }
            $validator = Validator::make($request->all(), [
                'name' => ["max:50",
                    Rule::unique('portals')->ignore($request->input('id'))],
                'public' => 'required|boolean',
                'status' => "required",
            ]);
            if($validator->fails()) {
                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);
            }else{
                $portal_id = $request->input('id');
                $portal = Portal::where('id',$portal_id)->first();
                if(!empty($portal)){
                    $portal->name = $request->input('name');
                    $portal->status = $request->input('status');
                    $portal->public = $request->input('public');
                    $portal->updated_at = Carbon::now();
                    $portal->save();

                    $updated_portal = Portal::where('id', $portal->id)->first();
                    $this->setStatusCode(JsonResponse::HTTP_OK);
                    return $this->customReposneWithStatusAndMessage($updated_portal, 'Portal Updated!');
                }else{
                    $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                    return $this->jsonResponseWithErrors("The Provided Portal does not Exists!");
                }

            }
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    //get portal by id
    public function getPortalById($id){
        try{
            $portal = Portal::where('id',$id)->first();
            if(!empty($portal)){
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->customReposneWithStatusAndMessage($portal, 'Portal Found!');
            }else{
                $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                return $this->jsonResponseWithErrors("The Provided Portal does not Exists!");
            }
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }


    //unit of measurment calls
    //get all unit of measure
    public function getAllUnitOfMeasurement(){
        try{
            $uom = Uom::with('portal')->get();
            $this->setStatusCode(JsonResponse::HTTP_OK);
            return $this->customReposneWithStatusAndMessage($uom, 'Uom Found');
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    //delete a specific unit of measure
    public function deleteUnitOfMeasurement($id){
        try{
            $uom = Uom::where('id', $id)->first();
            if(!empty($uom)){
                $uom->delete($id);
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->jsonResponseWithMessage("Unit of measure Deleted: ".$uom->name);
            }else{
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Provided Unit of measure does not Exists!");
            }
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    //add new unit of measure
    public function addNewUnitOfMeasurement(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:50',
                'public' => 'required|boolean',
                'portal' => 'required',
            ]);
            if($validator->fails()) {
                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);
            }else{
                $link_to_portal_id = $request->input('portal');
                $unit_of_measure = new Uom;
                $unit_of_measure->name = $request->input('name');
                $unit_of_measure->public = $request->input('public');
                $unit_of_measure->created_at = Carbon::now();
                $unit_of_measure->updated_at = Carbon::now();
                $unit_of_measure->save();

                $new_unit_of_measure = Uom::where('id',$unit_of_measure->id)->first();
                $new_unit_of_measure->portal()->sync($link_to_portal_id);
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->customReposneWithStatusAndMessage($new_unit_of_measure, 'Unit of measure Added');
            }
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    //update unit of measure
    public function updateUnitOfMeasurement(Request $request){
        try{
            if(empty($request->input('id')) ){
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Unit of measure ID must be provided!");
            }
            $validator = Validator::make($request->all(), [
                'name' => ["max:50",
                    Rule::unique('uoms')->ignore($request->input('id'))],
                'public' => 'required|boolean',
                'portal' => 'required',
            ]);
            if($validator->fails()) {
                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);
            }else{
                $unit_of_measurement_id = $request->input('id');
                $portal_ids = $request->input('portal');
                $unit_of_measure = Uom::where('id', $unit_of_measurement_id)->first();
                if(!empty($unit_of_measure)){
                    $unit_of_measure->name = $request->input('name');
                    $unit_of_measure->public = $request->input('public');
                    $unit_of_measure->updated_at = Carbon::now();
                    $unit_of_measure->save();

                    $updated_unit_of_measure = Uom::where('id', $unit_of_measure->id)->first();
                    $updated_unit_of_measure->portal()->sync($portal_ids);
                    $this->setStatusCode(JsonResponse::HTTP_OK);
                    return $this->customReposneWithStatusAndMessage($updated_unit_of_measure, 'Unit of measure Updated!');
                }else{
                    $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                    return $this->jsonResponseWithErrors("The Provided Unit of measurement does not Exists!");
                }
            }
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    //get unit of measure by id
    public function getUnitOfMeasurementById($id){
        try{
            $unit_of_measure = Uom::where('id',$id)->with('portal')->first();
            if(!empty($unit_of_measure)){
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->customReposneWithStatusAndMessage($unit_of_measure, 'Unit of measure Found!');
            }else{
                $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                return $this->jsonResponseWithErrors("The Provided Unit of measure does not Exists!");
            }
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }


    //unit of measurment conversion calls
    //delete unit of measurment conversion
    public function deleteUnitOfMeasurementConversion($id){
        try{
            $unit_of_measurement_conversion = UomConversion::where('id', $id)->first();
            if(!empty($unit_of_measurement_conversion)){
                $unit_of_measurement_conversion->delete($id);
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->jsonResponseWithMessage("Unit of measurement conversion Deleted: ".$unit_of_measurement_conversion->name);
            }else{
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Provided Unit of measurement conversion does not Exists!");
            }
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    //get all unit of measurment conversion
    public function getAllUnitOfMeasurementConversion(){
        try{
            $unit_of_measurment_conversion = UomConversion::with('fromUnitOfMeasurment','toUnitOfMeasurment')->get();
            $this->setStatusCode(JsonResponse::HTTP_OK);
            return $this->customReposneWithStatusAndMessage($unit_of_measurment_conversion, 'Unit of measurement conversion Found');
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    //add new unit of measurment conversion
    public function addNewUnitOfMeasurementConversion(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'uom_from' => 'required',
                'uom_to' => 'required',
                'conversion_value' => 'required',
            ]);
            if($validator->fails()) {
                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);
            }else{
                $post_uom_from = $request->input('uom_from');
                $post_uom_to = $request->input('uom_to');
                $post_conversion_value = $request->input('conversion_value');

                $result_uom_from = Uom::where('id',$post_uom_from)->first();
                $result_uom_to = Uom::where('id',$post_uom_to)->first();

                if(!empty($result_uom_to) && !empty($result_uom_from)){
                    $db_uom_to = UomConversion::where('uom_from', $post_uom_from)->get()->pluck('uom_to')->toArray();
//                    $db_uom_from = UomConversion::where('uom_to', $post_uom_to)->get()->pluck('uom_from')->toArray();
                    if(!in_array($post_uom_to, $db_uom_to)){
                        $unit_of_measurement_conversion = new UomConversion;
                        $unit_of_measurement_conversion->uom_from = $post_uom_from;
                        $unit_of_measurement_conversion->uom_to = $post_uom_to;
                        $unit_of_measurement_conversion->conversion_value = $post_conversion_value;
                        $unit_of_measurement_conversion->created_at = Carbon::now();
                        $unit_of_measurement_conversion->updated_at = Carbon::now();
                        $unit_of_measurement_conversion->save();

                        $new_db_record = UomConversion::with('fromUnitOfMeasurment','toUnitOfMeasurment')->where('id',$unit_of_measurement_conversion->id)->first();
                        $this->setStatusCode(JsonResponse::HTTP_OK);
                        return $this->customReposneWithStatusAndMessage($new_db_record, 'Unit of conversion value added');
                    }else{
                        $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                        return $this->jsonResponseWithErrors("The Unit of measurement conversion value already defined!");
                    }
                }else{
                    $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                    return $this->jsonResponseWithErrors("The Unit of measurement conversion from or to Provided Is Not Found!");
                }
            }
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    //update unit of measurement conversion record
    public function updateUnitOfMeasurementConversion(Request $request){
        try{
            if( empty($request->input('id')) ){
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Phase ID must be provided!");
            }
            $validator = Validator::make($request->all(), [
                'conversion_value' => ["required",
                    Rule::unique('uom_conversions')->ignore($request->input('id'))],
            ]);
            if($validator->fails()) {
                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);
            }else{

                $post_unit_of_measurment_conversion_id = $request->input('id');
                $unit_of_measurment_conversion = UomConversion::where('id',$post_unit_of_measurment_conversion_id)->first();

                if(!empty($unit_of_measurment_conversion)){
                    $unit_of_measurment_conversion->conversion_value = $request->input('conversion_value');
                    $unit_of_measurment_conversion->updated_at = Carbon::now();
                    $unit_of_measurment_conversion->save();

                    $updated_phase =  UomConversion::where('id',$unit_of_measurment_conversion->id)->with('fromUnitOfMeasurment','toUnitOfMeasurment')->first();
                    $this->setStatusCode(JsonResponse::HTTP_OK);
                    return $this->customReposneWithStatusAndMessage($updated_phase, 'Unit of measuremnt conversion Updated!');

                }else{
                    $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                    return $this->jsonResponseWithErrors("The Unit of measurement conversion Provided Is Not Found!");
                }

                /*
                $post_unit_of_measurment_conversion_id = $request->input('id');
                $post_uom_from = $request->input('uom_from');
                $post_uom_to = $request->input('uom_to');
                $post_conversion_value = $request->input('conversion_value');

                $result_uom_from = Uom::where('id',$post_uom_from)->first();
                $result_uom_to = Uom::where('id',$post_uom_to)->first();

                if(!empty($result_uom_to) && !empty($result_uom_from)){
                    $db_uom_frrom = UomConversion::where('uom_to',$post_uom_to)
                        ->where('id','<>',$post_unit_of_measurment_conversion_id)
                        ->get()
                        ->pluck('uom_from')
                        ->toArray();
                    if(!in_array($post_uom_from, $db_uom_frrom)){
                        $unit_of_measurement_conversion = new UomConversion;
                        $unit_of_measurement_conversion->uom_from = $post_uom_from;
                        $unit_of_measurement_conversion->uom_to = $post_uom_to;
                        $unit_of_measurement_conversion->conversion_value = $post_conversion_value;
                        $unit_of_measurement_conversion->created_at = Carbon::now();
                        $unit_of_measurement_conversion->updated_at = Carbon::now();
                        $unit_of_measurement_conversion->save();
                    }else{
                        $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                        return $this->jsonResponseWithErrors("The Unit of measurement conversion value already defined!");
                    }
                }else{
                    $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                    return $this->jsonResponseWithErrors("The Unit of measurement conversion from or to Provided Is Not Found!");
                }
                */
            }
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    //get unit of measurment conversion by id
    public function getUnitOfMeasurementConversionById($id){
        try{
            $unit_of_measurment_conversion = UomConversion::with('fromUnitOfMeasurment','toUnitOfMeasurment')->where('id',$id)->first();
            if(!empty($unit_of_measurment_conversion)){
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->customReposneWithStatusAndMessage($unit_of_measurment_conversion, 'Unit of measurement conversion Found');
            }else{
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Unit of measurement conversion Provided Is Not Found!");
            }
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

}
