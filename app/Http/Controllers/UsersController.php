<?php

namespace App\Http\Controllers;

use App\Http\ObjectTransformation;
use App\Http\Requests\UserRequest;
use App\Http\Resources\RolesResource;
use App\Http\Resources\UsersCollection;
use App\Http\Resources\UsersResource;
use App\PropertyListing;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
//use Illuminate\Http\JsonResponse;
use App\User;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Validator;

class UsersController extends Controller
{
    use ObjectTransformation;

    public function __construct()
    {
//        $this->middleware('auth:api', ['except' => ['login']]);
    }


    public function login(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
                'password' => 'required'
            ]);
            if ($validator->fails()) {
                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);
            }else{
                $email = $request->input('email');
                $password = $request->input('password');
                $count = User::where('email',$email)->count();
                if($count > 0){
                    if($token = auth()->attempt(['email' => $email, 'password' => $password])){
                        $user_id = optional(auth()->user())->id;
                        $user = User::with('roles')->find($user_id);
                        if($user->account_status !='rejected' && $user->account_status != 'pending'){
                            $token = auth()->attempt(['email' => $email, 'password' => $password]);
                            $remember_token = $this->respondWithToken($token);
                            $user->remember_token = $token;
                            $user->save();
                            return (new UsersResource($user,JsonResponse::HTTP_OK,"User Found",true))
                                ->response()->setStatusCode(JsonResponse::HTTP_OK);
                        }else{
                            $this->setStatusCode(JsonResponse::HTTP_UNAUTHORIZED);
                            return $this->jsonResponseWithErrors('your account is not activated or must be rejected by admin');
                        }

                    }else{
                        $this->setStatusCode(JsonResponse::HTTP_UNAUTHORIZED);
                        return $this->jsonResponseWithErrors('email or password is incorrect');
                    }
                }else{
                    $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                    return $this->jsonResponseWithErrors('account not found register yourself first');
                }
            }
        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function logout(){
        try{
            auth()->logout();
            $this->setStatusCode(JsonResponse::HTTP_OK);
            return $this->jsonResponseWithMessage('successfully logged out');
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function me(){
        try{
            $user = auth()->user();
            return (new UsersResource($user, JsonResponse::HTTP_OK, "User Found"))
                ->response()->setStatusCode(JsonResponse::HTTP_OK);

        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function register(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'email' => 'required|email|unique:users',
                'password' => 'sometimes|min:6',
                'first_name' => 'required',
                'last_name' => 'required',
                'gender' => 'required',
                'avatar' => 'sometimes|file|mimes:jpeg,jpg,bmp,png,svg|max:200000',
            ]);
            if ($validator->fails()) {
                $errors = $validator->errors()->getMessages();
                $errors_response = [];
                foreach ($errors as $name => $errors_array) {
                    $errors_response[$name] = $errors_array[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_response);
            }else{
                $input = (array)$request->all();
                $user = User::create($input);
                $user->password = bcrypt($request->password);
                $user->activation_token = sha1($request->email);
                $user->assignRole('user');

                if($request->hasFile('avatar')){
                    $image = request()->file('avatar');
                    $file = Storage::disk('public')->put('users', $image);
                    insertImage($user->id,'user',$file);
                }
                $user->save();
                return (new UsersResource($user,JsonResponse::HTTP_OK,"User Registered Successfully"))
                    ->response()->setStatusCode(JsonResponse::HTTP_OK);
            }

        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }

    }

    public function updatePassword(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'sometimes|min:6',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors()->getMessages();
            $errors_response = [];
            foreach ($errors as $name => $errors_array) {
                $errors_response[$name] = $errors_array[0];
            }
            $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            return $this->jsonResponseWithErrors($errors_response);
        }else{
            $email= $request->email;
            $count = User::where('email',$email)->count();
            if($count > 0){
                $password = $request->password;
                $user =  User::where('email',$email)->first();
                $user->password = bcrypt($password);
                $user->save();
                return (new UsersResource($user, JsonResponse::HTTP_OK, "Your password is updated"))
                    ->response()->setStatusCode(JsonResponse::HTTP_OK);
            }else{
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors("your account does not exists.");
            }
        }
    }

    public function verification($email,$token){
        try{
            $count = User::where('email',$email)->count();
        if($count > 0){
            $user =  User::where('email',$email)->first();
            if($user->activation_token !='' && $user->activation_token == $token){
                $user->account_status = 'approved';
                $user->save();
                return (new UsersResource($user, JsonResponse::HTTP_OK, "Your account is approved"))
                    ->response()->setStatusCode(JsonResponse::HTTP_OK);
            }else{
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors("invalid token");
            }
        }else{
            $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            return $this->jsonResponseWithErrors("your account does not exists.");
        }
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }

    }


    public function getAllUsers(){
        $count = User::All()->count();
        if($count > 0){
            $users = User::role(['user','admin','dealer','superAdmin'])->get();
            return (new UsersCollection($users,JsonResponse::HTTP_OK,"All Users Found"))
                ->response()->setStatusCode(JsonResponse::HTTP_OK);
        }else{
            $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
            return $this->jsonResponseWithErrors("Users Not Found");
        }
    }

    public function changeUserStatusById($id,$status){
        if($id != ''){
            $count = User::where('id',$id)->count();
            if($count > 0){
                $user = User::where('id',$id)->first();
                $user->account_status = $status;
                $user->save();
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->jsonResponseWithMessage("Account Status Changed");
            }else{
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors("User Not Found");
            }
        }else{
            $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            return $this->jsonResponseWithErrors("Id not Provided");
        }
    }

    public function deleteUserById($userId){
        if($userId){
            $count = User::where('id',$userId)->count();
            if($count > 0){
                $user = User::where('id',$userId);
                $user->delete();
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->jsonResponseWithMessage("User Deleted Successfully");
            }else{
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors("User Not Found");
            }
        }else{
            $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            return $this->jsonResponseWithErrors("Id not Provided");
        }

    }

    public function getUserById($userId){
        if($userId){
            $count = User::where('id',$userId)->count();
            if($count > 0){
                $user = User::where('id',$userId)->first();
                return (new UsersResource($user, JsonResponse::HTTP_OK, "User Found"))
                    ->response()->setStatusCode(JsonResponse::HTTP_OK);
            }else{
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors("User Not Found");
            }
        }else{
            $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            return $this->jsonResponseWithErrors("Id not Provided");
        }
    }

    public function updateUserById(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
                'first_name' => 'required',
                'last_name' => 'required',
                'gender' => 'required',
            ]);
            if ($validator->fails()) {
                $errors = $validator->errors()->getMessages();
                $errors_response = [];
                foreach ($errors as $name => $errors_array) {
                    $errors_response[$name] = $errors_array[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_response);
            }else{
                $userid = $request->input('id');
                $count = User::where('id',$userid)->count();
                if($count > 0){
                    $user = User::where('id',$userid)->update($request->all());
                    if($user > 0){
                        $user = User::where('id',$userid)->first();
                        $this->setStatusCode(JsonResponse::HTTP_OK);
                        return (new UsersResource($user, JsonResponse::HTTP_OK, "User Updated Successfully'",true))
                            ->response()->setStatusCode(JsonResponse::HTTP_OK);
                    }else{
                        $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                        return $this->jsonResponseWithErrors("User Not Update Successfully");
                    }
                }else{
                    $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                    return $this->jsonResponseWithErrors("User Not Found");
                }
            }
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }



    /*
    *
    * TAIMOOR ALI
    *
    */

    public function getAllRoles(){
        try{
            $roles = Role::all();
            $data = $roles->map(function ($role){
                return $this->rolesObjectTransformation($role);
            });
            $this->setStatusCode(JsonResponse::HTTP_OK);
            return $this->customReposneWithStatusAndMessage($data,'Roles Found');
//            return $this->jsonResponse($data);
        } catch (\Exception $e){
            $this->setStatusCode(JsongetRolePermissionsResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function getAllPermissions(){
        try{
            $permissions = Permission::all();
            $data = $permissions->map(function ($permission){
                return $this->permissionObjectTransformation($permission);
            });
            $this->setStatusCode(JsonResponse::HTTP_OK);
            return $this->customReposneWithStatusAndMessage($data,'Permissions Found');
        } catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function getRolePermissions($role){
        try{
            $permissions = Role::where('id',$role)->with('permissions')->first();
            return (new RolesResource($permissions,JsonResponse::HTTP_OK,'found',true))
                ->response()->setStatusCode(JsonResponse::HTTP_OK);

        } catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function updateRolePermissions(Request $request,$role_id){
        try{
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'permissions' => 'array'
            ]);
            if ($validator->fails()) {
                $errors = $validator->errors()->getMessages();
                $errors_response = [];
                foreach ($errors as $name => $errors_array) {
                    $errors_response[$name] = $errors_array[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_response);
            }else{
                DB::beginTransaction();
                $role = Role::findOrFail($role_id);
                $role->update(['name'=>$request->input('name')]);
                $role->permissions()->sync($request->input('permissions'));
                DB::commit();
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->jsonResponseWithMessage("Permissions Updated Successfully");
            }

        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }


//    public function setRolePermissions(Request $request){
//        try{
//
//            $validator = Validator::make($request->all(), [
//                'role_name' => 'required',
//                'permissions' => 'required'
//            ]);
//
//            $request = $request->all();
//            $role = $request['role_name'];
//            $role = Role::findByName($role);
//            $requestPermissions = $request['permissions'];
//            $rolePermissions = $role->permissions->pluck('name','name')->all();
//            $allPermissions = Permission::all()->pluck('name','name')->all();
//
//            $messaage = "";
//
//
//            foreach ($requestPermissions as $permission) {
//                if(in_array($permission, $allPermissions)){
//                    if(!in_array($permission, $rolePermissions)){
//                        $role->givePermissionTo($permission);
//                        $messaage .= "Permission Assigned Successfully: " . $permission . "! ";
//                    } else {
//                        $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
//                        return $this->jsonResponseWithErrors("Already Assigned Permission: ".$permission);
//                    }
//                } else {
//                    $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
//                    return $this->jsonResponseWithErrors("Invalid Permission: ".$permission);
//                }
//            }
//
//            $this->setStatusCode(JsonResponse::HTTP_OK);
//            return $this->jsonResponseWithMessage($messaage);
//
//        } catch (\Exception $e){
//            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
//            return $this->jsonResponseWithErrors($e->getMessage());
//        }
//    }

//    public function removeRolePermissions(Request $request){
//        try{
//
//            $validator = Validator::make($request->all(), [
//                'role_name' => 'required',
//                'permissions' => 'required'
//            ]);
//
//            $request = $request->all();
//            $role = $request['role_name'];
//            $role = Role::findByName($role);
//            $requestPermissions = $request['permissions'];
//            $rolePermissions = $role->permissions->pluck('name','name')->all();
//            $allPermissions = Permission::all()->pluck('name','name')->all();
//
//            $messaage = "";
//
//            foreach ($requestPermissions as $permission) {
//                if(in_array($permission, $allPermissions)){
//                    if(in_array($permission, $rolePermissions)){
//                        $role->revokePermissionTo($permission);
//                        $messaage .= "Permission Removed Successfully: " . $permission . "! ";
//                    } else {
//                        $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
//                        return $this->jsonResponseWithErrors("Unassociated Permission: ".$permission."!");
//                    }
//                } else {
//                    $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
//                    return $this->jsonResponseWithErrors("Invalid Permission: ".$permission);
//                }
//            }
//
//            $this->setStatusCode(JsonResponse::HTTP_OK);
//            return $this->jsonResponseWithMessage($messaage);
//
//        } catch (\Exception $e){
//            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
//            return $this->jsonResponseWithErrors($e->getMessage());
//        }
//    }

    public function getUserRole($user_id){
        try{
            $user = User::where("id",$user_id)->with('roles')->first();
            if(!empty($user)){
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->jsonResponse($user->roles);
            }
            $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            return $this->jsonResponseWithErrors("User Not Found!");
        } catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function setUserRole(Request $request){
        try{

            $validator = Validator::make($request->all(), [
                'user_id' => 'required',
                'role_name' => 'required'
            ]);

            $request = $request->all();
            $role = $request["role_name"];

            $roles = Role::pluck('name','name')->all();
            $user = User::where("id",$request["user_id"])->first();

            if(!empty($user)){
                // check if the role is valid
                if(in_array($role, $roles)){
                    // if role is already assigned
                    if($user->hasRole($role)){
                        $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                        return $this->jsonResponseWithErrors("Role Already Assigned!");
                    } 
                    // else assign the role
                    else {
                        $user->assignRole($role);
                        $this->setStatusCode(JsonResponse::HTTP_OK);
                        return $this->jsonResponseWithMessage("Role Assigned Successfully!");
                    }
                } else {
                    $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                    return $this->jsonResponseWithErrors("Invalid Role!");
                }
            }
            $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            return $this->jsonResponseWithErrors("User Not Found!");
        } catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function removeUserRole(Request $request){
        try{

            $validator = Validator::make($request->all(), [
                'user_id' => 'required',
                'role_name' => 'required'
            ]);

            $request = $request->all();
            $role = $request["role_name"];

            $roles = Role::pluck('name','name')->all();
            $user = User::where("id",$request["user_id"])->first();

            if(!empty($user)){
                // check if the role is valid
                if(in_array($role, $roles)){
                    // if role is already assigned
                    if($user->hasRole($role)){
                        $user->removeRole($role);
                        $this->setStatusCode(JsonResponse::HTTP_OK);
                        return $this->jsonResponseWithMessage("Role Removed Successfully!");
                    } else {
                        $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                        return $this->jsonResponseWithErrors("Unassociated Role: ".$role);
                    }
                } else {
                    $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                    return $this->jsonResponseWithErrors("Invalid Role!");
                }
            }
            $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            return $this->jsonResponseWithErrors("User Not Found!");
        } catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function getAllUserCount(){
        try{
            $users = User::all()->count();
            $this->setStatusCode(JsonResponse::HTTP_OK);
            return $this->customReposneWithStatusAndMessage($users, 'Users Count!');
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

}
