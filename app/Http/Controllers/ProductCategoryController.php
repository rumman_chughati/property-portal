<?php

namespace App\Http\Controllers;

use App\Attribute;
use App\Http\ObjectTransformation;
use App\Portal;
use App\Prodcat;
use App\ProdcatAttribute;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\Rule;


class ProductCategoryController extends Controller
{
    use ObjectTransformation;
    /*
     *  Product Category
     */

    public function deleteProductCategory($id){
        try{
            $product_category = Prodcat::where('id', $id)->first();
            if(!empty($product_category)){
                $this->recursiveProdcatDelete($id);
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->jsonResponseWithMessage("Product Category Deleted: ".$product_category->name);
            } else {
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Provided Product Category does not Exists!");
            }
        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function recursiveProdcatDelete($prodcatid){
        $childs = Prodcat::where('parent_id',$prodcatid)->get()->pluck('id');
        $count = sizeof($childs);
        if($count > 0){
            foreach ($childs as $c){
                $this->recursiveProdcatDelete($c);
            }
        }
        Prodcat::destroy([$prodcatid]);
    }

    public function getAllProductCategory(){
        try{
            $product_category = Prodcat::with('portal','prodcatParent')->get();
            if(!empty($product_category)){
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->customReposneWithStatusAndMessage($product_category, 'Product Category Found');
            }else{
                $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                return $this->jsonResponseWithErrors("No Product Category Found!");
            }
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function addNewProductCategory(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'name' => 'required|unique:prodcats',
//                'level' => 'required|max:65535',
                'public' => 'required|boolean',
//                'leaf_node' => 'required|boolean',
                'service' => 'required|boolean',
            ]);
            if($validator->fails()) {
                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);
            }else{
                $product_category = new Prodcat;
                $product_category->name = $request->input('name');
                $product_category->level = 0;
                $product_category->service = $request->input('service');
                $product_category->public = $request->input('public');
                $product_category->created_at = Carbon::now();
                $product_category->updated_at = Carbon::now();

                $product_category->save();

                $new_prodcat = Prodcat::where('id',$product_category->id)->with('portal')->first();

                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->customReposneWithStatusAndMessage($new_prodcat,'Product Category Added!');
            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function updateProductCategory(Request $request){
        try{

            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'name' => ["required",
                    Rule::unique('prodcats')->ignore($request->input('id'))],
                'public' => 'required|boolean',
                'service' => 'required|boolean',
            ]);

            if($validator->fails()) {
                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);

            }else{

                $product_category = Prodcat::where('id', $request->input('id'))->first();
                if(!empty($product_category)){
                    //type update
                    $product_category->name = $request->input('name');
                    $product_category->public = $request->input('public');
                    $product_category->service = $request->input('service');
                    $product_category->updated_at = Carbon::now();
                    $product_category->save();


                    $updated_prodcat = Prodcat::where('id', $product_category->id)->with('portal')->first();

                    $this->setStatusCode(JsonResponse::HTTP_OK);
                    return $this->customReposneWithStatusAndMessage($updated_prodcat, 'Prodcat Updated!');

                } else {
                    $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                    return $this->jsonResponseWithErrors("The Provided Product Category does not Exists!");
                }
            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function getProductCategoryById($id){
        try{
            if($id != ''){
                $product_category = Prodcat::where('id',$id)->with('portal')->first();
                if(!empty($product_category)){
                    $this->setStatusCode(JsonResponse::HTTP_OK);
                    return $this->customReposneWithStatusAndMessage($product_category, 'Product Category Found');
                }else{
                    $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                    return $this->jsonResponseWithErrors("Product Category Not Found!");
                }
            }else{
                $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                return $this->jsonResponseWithErrors("Product Cateogry ID is not Provided!");
            }
        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function linkProdcatWithPortal(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'portal' => 'required',
            ]);
            if($validator->fails()) {
                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);
            }else{
                $prodcat_id = $request->input('id');
                $portals = $request->input('portal');
                $prodcat = Prodcat::where('id',$prodcat_id)->with('portal')->first();
                $prodcat->portal()->sync($portals);
                $prodcat = Prodcat::where('id',$prodcat_id)->with('portal')->first();
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->customReposneWithStatusAndMessage($prodcat, 'Prodcat Linked To Portals');
            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    /*
     *  Attributes
    */

    public function linkAttributeWithProdcat(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'prodcat' => 'required',
            ]);
            if($validator->fails()) {
                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);
            }else{
                $prodcat_id = $request->input('id');
                $portals = $request->input('portal');
                $prodcat = Prodcat::where('id',$prodcat_id)->first();
                $prodcat->portal()->sync($portals);
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->customReposneWithStatusAndMessage($prodcat, 'Prodcat Linked To Portals');
            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function deleteAttribute($id){
        try{
            $attribute = Attribute::where('id', $id)->first();
            if(!empty($attribute)){
                //type deletion
                $attribute->delete($id);
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->jsonResponseWithMessage("Attribute Deleted: ".$attribute->name);
            } else {
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Provided Attribute does not Exists!");
            }
        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function getAllAttributes(){
        try{
            $attributes = Attribute::get();
            if(!empty($attributes)){
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->customReposneWithStatusAndMessage($attributes, 'Attributes Found');
            }else{
                $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                return $this->jsonResponseWithErrors("No Attributes Found!");
            }
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function addNewAttribute(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:50|unique:attributes,name',
                'type' => 'required'
            ]);

            if($validator->fails()) {
                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);
            }else{
                $attribute = new Attribute();
                $attribute->name = $request->input('name');
                $attribute->type = $request->input('type');
                $attribute->created_at = Carbon::now();
                $attribute->updated_at = Carbon::now();
                $attribute->save();

                $new_attribute = Attribute::where('id',$attribute->id)->first();
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->customReposneWithStatusAndMessage($new_attribute, 'Attribute Added');
            }
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function updateAttribute(Request $request){
        try{
            if(empty($request->input('id')) ){
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Attribute ID must be provided!");
            }
            $validator = Validator::make($request->all(), [
                'name' => ["max:50",
                    Rule::unique('attributes')->ignore($request->input('id'))],
                'type' => 'required',
            ]);
            if($validator->fails()) {
                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);
            }else{
                $attribute_id = $request->input('id');
                $attribute = Attribute::where('id',$attribute_id)->first();
                if(!empty($attribute)){
                    $attribute->name = $request->input('name');
                    $attribute->type = $request->input('type');
                    $attribute->updated_at = Carbon::now();
                    $attribute->save();

                    $updated_attribute = Attribute::where('id', $attribute->id)->first();
                    $this->setStatusCode(JsonResponse::HTTP_OK);
                    return $this->customReposneWithStatusAndMessage($updated_attribute, 'Attribute Updated!');
                }else{
                    $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                    return $this->jsonResponseWithErrors("The Provided Attribute does not exists!");
                }

            }
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function getAttributeById($id){
        try{
            $attribute = Attribute::where('id',$id)->first();
            if(!empty($attribute)){
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->customReposneWithStatusAndMessage($attribute, 'Attribute Found!');
            }else{
                $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                return $this->jsonResponseWithErrors("The Provided Attribute does not exists!");
            }
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    /*
     *  Prodcat Attributes
    */
    public function deleteProdcatAttribute($id){
        try{
            $prodcatAttribute = ProdcatAttribute::where('id', $id)->with('attribute','prodcat')->first();
            if(!empty($prodcatAttribute)){
                //type deletion
                $prodcatAttribute->delete($id);
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->jsonResponseWithMessage("Prodcat Attribute Deleted: ".$prodcatAttribute->name);
            } else {
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Provided Prodcat Attribute does not Exists!");
            }
        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function getAllProdcatAttributes(){
        try{
            $prodcat_attributes = ProdcatAttribute::with('attribute','prodcat')->get();
            if(!empty($prodcat_attributes)){
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->customReposneWithStatusAndMessage($prodcat_attributes, 'Prodcat Attributes Found');
            }else{
                $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                return $this->jsonResponseWithErrors("No Prodcat Attributes Found!");
            }
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function getProdcatAttributeById($id){
        try{
            $prodcat_attribute = ProdcatAttribute::where('id',$id)->with('attribute','prodcat')->first();
            if(!empty($prodcat_attribute)){
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->customReposneWithStatusAndMessage($prodcat_attribute, 'Prodcat Attribute Found!');
            }else{
                $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                return $this->jsonResponseWithErrors("The Provided Prodcat Attribute does not exists!");
            }
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function addNewProdcatAttribute(Request $request){
        try{

            $attribute_id = $request->input('attribute_id');
            $attribute = Attribute::where('id',$attribute_id)->first();
            $rules['prodcat_id'] = "required";
            $rules['attribute_id'] = "required";
            if($attribute->type == 'number'){
                $rules['prodcat_attribute_val'] = array("required","numeric");
            }
            if($attribute->type == 'char'){
                $rules['prodcat_attribute_val'] = "required";
            }
            if($attribute->type == 'boolean'){
                $rules['prodcat_attribute_val'] = "required";
            }
            $validator = Validator::make($request->all(), $rules);
            if($validator->fails()) {
                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);
            }else{
                $prodcat_id = $request->input('prodcat_id');
                $prodcat = Prodcat::where('id',$prodcat_id)->first();

                if($prodcat->leaf_node){
                    $prodcat_attribute = new ProdcatAttribute();
                    $prodcat_attribute->prodcat_attribute_val = $request->input('prodcat_attribute_val');
                    $prodcat_attribute->prodcat_id = $request->input('prodcat_id');
                    $prodcat_attribute->attribute_id = $request->input('attribute_id');
                    $prodcat_attribute->created_at = Carbon::now();
                    $prodcat_attribute->updated_at = Carbon::now();
                    $prodcat_attribute->save();
                }else{
                    $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                    return $this->jsonResponseWithErrors("The Provided Prodcat is not a leaf node!");
                }

                $new_attribute = ProdcatAttribute::where('id',$prodcat_attribute->id)->with('attribute','prodcat')->first();
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->customReposneWithStatusAndMessage($new_attribute, 'Attribute Added');
            }
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function updateProdcatAttribute(Request $request){
        try{
            if(empty($request->input('id')) ){
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Prodcat Attribute ID must be provided!");
            }
            $attribute_id = $request->input('attribute_id');
            $attribute = Attribute::where('id',$attribute_id)->first();
            $rules['prodcat_id'] = "required";
            $rules['attribute_id'] = "required";
            if($attribute->type == 'number'){
                $rules['prodcat_attribute_val'] = array("required","numeric");
            }
            if($attribute->type == 'char'){
                $rules['prodcat_attribute_val'] = "required";
            }
            if($attribute->type == 'boolean'){
                $rules['prodcat_attribute_val'] = "required";
            }
            $validator = Validator::make($request->all(),$rules);
            if($validator->fails()) {
                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);
            }else{
                $prodcat_id = $request->input('prodcat_id');
                $prodcat = Prodcat::where('id',$prodcat_id)->first();

                if($prodcat->leaf_node){
                    $prodcat_attribute_id = $request->input('id');
                    $prodcat_attribute = ProdcatAttribute::where('id',$prodcat_attribute_id)->first();
                    if(!empty($prodcat_attribute)){
                        $prodcat_attribute->prodcat_id = $request->input('prodcat_id');
                        $prodcat_attribute->attribute_id = $request->input('attribute_id');
                        $prodcat_attribute->prodcat_attribute_val = $request->input('prodcat_attribute_val');
                        $prodcat_attribute->updated_at = Carbon::now();
                        $prodcat_attribute->save();

                        $updated_prodcat_attribute = ProdcatAttribute::where('id', $prodcat_attribute->id)->with('attribute','prodcat')->first();
                        $this->setStatusCode(JsonResponse::HTTP_OK);
                        return $this->customReposneWithStatusAndMessage($updated_prodcat_attribute, 'Prodcat Attribute Updated!');
                    }else{
                        $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                        return $this->jsonResponseWithErrors("The Provided Prodcat Attribute does not exists!");
                    }
                }else{
                    $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                    return $this->jsonResponseWithErrors("The Provided Prodcat is not a leaf node!");
                }



            }
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    /*
     * Prodcat Tree Calls
     *
     * */

    public function updateProdcatTree(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'children_id' => 'required',
            ]);
            if($validator->fails()) {
                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);
            }else{
                $levelParent = 0;
                $product_category = Prodcat::where('id', $request->input('children_id'))->first();
                $last_parent_id = $product_category->parent_id;
                $children_id = $product_category->id;
                if($request->input('parent_id') != null){
                    $product_category->parent_id = $request->input('parent_id');
                    $update_parent_leaf = Prodcat::where('id', $request->input('parent_id'))->first();
                    $update_parent_leaf->save();

                }else{
                    $product_category->parent_id = null;
                }
                $product_category->updated_at = Carbon::now();

                $product_category->save();

                if($request->input('parent_id') != null){
                    $main_parent = $this->tracebackParentId($product_category->parent_id);
                    $this->changeLevel($main_parent,$levelParent);
                }else{
                    $main_parent = $this->tracebackParentId($last_parent_id);
                    $this->changeLevel($main_parent,$levelParent);
                    $this->changeLevel($children_id,$levelParent);
                }
                $product_category = Prodcat::with('portal')->get();
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->customReposneWithStatusAndMessage($product_category,'Prodcat Moved Successfully!');
            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function tracebackParentId($parentid){
        $parent = Prodcat::where('id',$parentid)->first();
        $found_id = 0;
        if($parent->parent_id != ''){
            $found_id = $parent->parent_id;
            return $this->tracebackParentId($found_id);
        }elseif ($parent->parent_id == ''){
            $found_id = $parentid;
            return $found_id;
        }
    }

    public function changeLevel($parentid,$level){
        $parent = Prodcat::where('id',$parentid)->first();
        $parent->level = $level;
        $parent->save();
        $child = Prodcat::where('parent_id',$parentid)->get();
        $count = sizeof($child);
        $childLevel = $parent->level + 1;
        if($count > 0){
            foreach ($child as $c){
                $updateChild = Prodcat::where('id',$c->id)->first();
                $updateChild->level = $childLevel;
                $updateChild->save();
                $this->checkChild($c->id,$childLevel);
            }
            Prodcat::where('id',$parentid)->update(['leaf_node'=>false]);
        }elseif ($count == 0){
            Prodcat::where('id',$parentid)->update(['leaf_node'=>true]);
        }

    }

    public function checkChild($parentid,$level){
        $child = Prodcat::where('parent_id',$parentid)->get();
        $count = sizeof($child);
        $current_level = $level + 1;
        if($count > 0){
            foreach ($child as $c){
                $update = Prodcat::where('id',$c->id)->first();
                $update->level = $current_level;
                $update->save();
                $this->checkChild($c->id,$current_level);
            }
            Prodcat::where('id',$parentid)->update(['leaf_node'=>false]);
        }elseif ($count == 0){
            Prodcat::where('id',$parentid)->update(['leaf_node'=>true]);
        }
    }


    public function getProdcatByPortalId($portalid){
        try{
            $result = Portal::with('prodcat')->find($portalid);
            $this->setStatusCode(JsonResponse::HTTP_OK);
            return $this->customReposneWithStatusAndMessage($result['prodcat'],'Protal Prodcat Found!');
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }
}
