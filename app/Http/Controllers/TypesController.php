<?php

namespace App\Http\Controllers;

use App\DealType;
use App\Http\ObjectTransformation;
use App\Http\Resources\PropertyTypeResource;
use App\Http\Resources\PropertyTypesCollection;
use App\PropertyClass;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Validator;
use Illuminate\Validation\Rule;


class TypesController extends Controller
{
    use ObjectTransformation;

    /*
     *  Property Types
     */
    public function deletePropertyClassById($id){
        try{

            $class = PropertyClass::where('id', $id)->where('parent_id', Null)->first();
            if(!empty($class)){

                //sub-types deletion
                $types = PropertyClass::where('parent_id', $id)->get()->pluck('id');
                PropertyClass::destroy($types);

                //type deletion
                $class->delete($id);

                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->jsonResponseWithMessage("Property Class Deleted: ".$class->name);

            } else {
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Provided Property Class does not Exists!");
            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function getAllPropertyClasses(){
        try{
            $class = PropertyClass::where('parent_id',null)->with('subTypes')->get();
            if(!empty($class)){
                return (new PropertyTypesCollection($class,JsonResponse::HTTP_OK,"Types Found!",true))
                    ->response()->setStatusCode(JsonResponse::HTTP_OK);
            }else{
                $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                return $this->jsonResponseWithErrors("No Property Type Found!");
            }

        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function addNewPropertyClass(Request $request){
        try{

            $validator = Validator::make($request->all(), [
                'name' => 'required|unique:property_classes',
                'public' => 'required|boolean',
            ]);

            if($validator->fails()) {

                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);

            }else{

                $propertyClass = new PropertyClass;
                $propertyClass->name = $request->input('name');
                $propertyClass->public = $request->input('public');
                $propertyClass->created_at = Carbon::now();
                $propertyClass->updated_at = Carbon::now();

                $propertyClass->save();

                return (new PropertyTypeResource($propertyClass, JsonResponse::HTTP_OK, "Property Class Added!", true))
                    ->response()->setStatusCode(JsonResponse::HTTP_OK);

            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function updatePropertyClassById(Request $request){
        try{

            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'name' => ["required",
                    Rule::unique('property_classes')->ignore($request->input('id'))],
                'public' => 'required|boolean'
            ]);

            if($validator->fails()) {

                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);

            }else{

                $class = PropertyClass::where('id', $request->input('id'))->where('parent_id', Null)->first();
                if(!empty($class)){

                    //type update
                    $class->name = $request->input('name');
                    $class->public = $request->input('public');
                    $class->updated_at = Carbon::now();
                    $class->save();

                    $this->setStatusCode(JsonResponse::HTTP_OK);
                    return $this->jsonResponseWithMessage("Property Class Updated: ".$class->name);

                } else {
                    $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                    return $this->jsonResponseWithErrors("The Provided Property Class does not Exists!");
                }
            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function getPropertyClassById($id){
        try{
            if($id != ''){
                $class = PropertyClass::where('id',$id)->with('subTypes')->first();
                if(!empty($class)){

                    return (new PropertyTypeResource($class, JsonResponse::HTTP_OK, "Property Class Found!", true))
                        ->response()->setStatusCode(JsonResponse::HTTP_OK);

                }else{
                    $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                    return $this->jsonResponseWithErrors("Property Class Not Found!");
                }
            }else{
                $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                return $this->jsonResponseWithErrors("Property Class ID is not Provided!");
            }
        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    /*
     *  Property Sub-Types
     */
    public function deletePropertyTypeById($id){
        try{
            $type = PropertyClass::where('id',$id)->where('parent_id', '<>', Null)->first();
            if(!empty($type)){
                $type->delete($type->id);
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->jsonResponseWithMessage("Property Type Deleted: ".$type->name);
            } else {
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Provided Property Type does not Exists!");
            }
        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function addNewPropertyType(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'name' => 'required|unique:property_classes',
                'public' => 'required|boolean',
            ]);
            if($validator->fails()) {

                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);

            }else{

                $class = PropertyClass::where('id', $request->input('id'))->where('parent_id',null)->first();
                if(!empty($class)){

                    $type = new PropertyClass;

                    $type->parent_id = $request->input('id');
                    $type->name = $request->input('name');
                    $type->public = $request->input('public');
                    $type->created_at = Carbon::now();
                    $type->updated_at = Carbon::now();

                    $type->save();

                    $this->setStatusCode(JsonResponse::HTTP_OK);
                    return $this->customReposneWithStatusAndMessage($type,'Property Type Added!');

                } else {
                    $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                    return $this->jsonResponseWithErrors("The Provided Property Class does not Exists!");
                }
            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function updatePropertyTypeById(Request $request){
        try{

            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'name' => ["required",
                    Rule::unique('property_classes')->ignore($request->input('id'))],
                'public' => 'required|boolean'
            ]);

            if($validator->fails()) {

                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);

            }else{

                $type = PropertyClass::where('id', $request->input('id'))->where('parent_id', '<>', Null)->first();
                if(!empty($type)){

                    $type->name = $request->input('name');
                    $type->public = $request->input('public');
                    $type->updated_at = Carbon::now();
                    $type->save();

                    $this->setStatusCode(JsonResponse::HTTP_OK);
                    return $this->customReposneWithStatusAndMessage($type,'Property Type Updated!');

                } else {
                    $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                    return $this->jsonResponseWithErrors("The Provided Property Type does not Exists!");
                }
            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }


    /*
     * Deal Type
    */

    //delete deal type
    public function deleteDealType($id){
        try{
            $deal_type = DealType::where('id', $id)->first();
            if(!empty($deal_type)){
                $deal_type->delete($id);
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->jsonResponseWithMessage("Deal Type Deleted: ".$deal_type->name);
            }else{
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Provided Deal type does not Exists!");
            }
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    //getAllDealType
    public function getAllDealType(){
        try{
            $deal_type = DealType::get();
            $this->setStatusCode(JsonResponse::HTTP_OK);
            return $this->customReposneWithStatusAndMessage($deal_type, 'Deal Type Found');
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    //add new dealtype
    public function addNewDealType(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:50|unique:deal_types,name',
                'public' => 'required|boolean'
            ]);

            if($validator->fails()) {
                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);
            }else{
                $deal_type = new DealType();
                $deal_type->name = $request->input('name');
                $deal_type->public = $request->input('public');
                $deal_type->created_at = Carbon::now();
                $deal_type->updated_at = Carbon::now();
                $deal_type->save();

                $new_deal_type = DealType::where('id',$deal_type->id)->first();
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->customReposneWithStatusAndMessage($new_deal_type, 'Deal Type Added');
            }
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    //update a deal type
    public function updateDealType(Request $request){
        try{
            if(empty($request->input('id')) ){
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Deal type ID must be provided!");
            }
            $validator = Validator::make($request->all(), [
                'name' => ["max:50",
                    Rule::unique('deal_types')->ignore($request->input('id'))],
                'public' => 'required|boolean',
            ]);
            if($validator->fails()) {
                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);
            }else{
                $deal_type_id = $request->input('id');
                $deal_type = DealType::where('id',$deal_type_id)->first();
                if(!empty($deal_type)){
                    $deal_type->name = $request->input('name');
                    $deal_type->public = $request->input('public');
                    $deal_type->updated_at = Carbon::now();
                    $deal_type->save();

                    $updated_deal_type = DealType::where('id', $deal_type->id)->first();
                    $this->setStatusCode(JsonResponse::HTTP_OK);
                    return $this->customReposneWithStatusAndMessage($updated_deal_type, 'Deal Type Updated!');
                }else{
                    $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                    return $this->jsonResponseWithErrors("The Provided Deal Type does not exists!");
                }

            }
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    //get specific deal type by id
    public function getDealTypeById($id){
        try{
            $deal_type = DealType::where('id',$id)->first();
            if(!empty($deal_type)){
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->customReposneWithStatusAndMessage($deal_type, 'Deal Type Found!');
            }else{
                $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                return $this->jsonResponseWithErrors("The Provided Deal type does not exists!");
            }
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

}
