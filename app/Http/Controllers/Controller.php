<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $status_code = JsonResponse::HTTP_OK;

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->status_code;
    }

    /**
     * @param int $status_code
     */
    public function setStatusCode(int $status_code): void
    {
        $this->status_code = $status_code;
    }

    public function jsonResponse($data, $metaData=[])
    {
        return response()->json($data, $this->getStatusCode(), $metaData);
    }

    public function jsonResponseWithMessage($message=''){
        return response()->json([
           'message' => $message,
            'status_code' => $this->getStatusCode(),
        ]);
    }

    public function jsonResponseWithErrors($message){
        return $this->jsonResponse([
            'status_code' => $this->getStatusCode(),
            'errors' => $message,
        ]);
    }

    public function respondWithToken($token){
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

    public function refreshJwtToken(){
        return $this->respondWithToken(auth()-refresh());
    }

    public function customReposneWithStatusAndMessage($data,$message){
        return response()->json([
            'data' => $data,
            'status_code' => $this->getStatusCode(),
            'message' => $message,
        ]);

    }

    // helper method to get enum values of a table's field: by Taimoor
    function getEnumValues( $table, $field ){
        $type = DB::select(DB::raw("SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'"))[0]->Type;
        preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
        $enum = explode("','", $matches[1]);
        return $enum;
    }


}
