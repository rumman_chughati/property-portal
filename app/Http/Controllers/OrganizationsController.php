<?php

namespace App\Http\Controllers;

use App\City;
use App\Http\ObjectTransformation;
use App\Http\Resources\OrganizationsCollection;
use App\Http\Resources\OrganizationResource;
use App\LocationContactPerson;
use App\OrganizationLocation;
use App\OrganizationType;
use App\Organization;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class OrganizationsController extends Controller
{
    use ObjectTransformation;

    /*
     *  Organizations
     */
    public function deleteOrganizationById($id){
        try{
            $organization = Organization::where('id', $id)->first();
            if(!empty($organization)){

                $organization->delete($id);

                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->jsonResponseWithMessage("Organization Deleted: ".$organization->name);

            } else {
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Provided Organization does not Exists!");
            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function getAllOrganizations(){
        try{

            $organizations = Organization::with(['type', 'locations'])->get();

            //type and locations
            return (new OrganizationsCollection($organizations,JsonResponse::HTTP_OK,"Organizations Found!", true, true))
                ->response()->setStatusCode(JsonResponse::HTTP_OK);

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function addNewOrganization(Request $request){
        try{

            $validator = Validator::make($request->all(), [
                'name' => 'required|max:50|unique:organizations,name',
                'organization_type_id' => 'required',
                'registration_id' => 'required|max:20|unique:organizations,registration_id',
                'uan' => 'digits_between:1,10|unique:organizations,uan',
                'ntn' => 'required|string|max:20|unique:organizations,ntn',
                'stn' => 'required|string|max:20|unique:organizations,stn',
                'verified' => 'required|boolean'
            ]);

            if($validator->fails()) {

                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);

            }else{
                $type = OrganizationType::where('id', $request->input('organization_type_id'))->first();
                if( !empty($type) ) {
                    $organization = new Organization;
                    $organization->name = $request->input('name');
                    $organization->organization_type_id = $request->input('organization_type_id');
                    $organization->registration_id = $request->input('registration_id');
                    $organization->uan = $request->input('uan');
                    $organization->ntn = $request->input('ntn');
                    $organization->stn = $request->input('stn');
                    $organization->verified = $request->input('verified');

                    $organization->created_at = Carbon::now();
                    $organization->updated_at = Carbon::now();

                    $organization->save();

                    $new_organization = Organization::where('id', $organization->id)->with('type')->first();

                    $this->setStatusCode(JsonResponse::HTTP_OK);
                    return $this->customReposneWithStatusAndMessage($new_organization, 'Organization Added!');

                } else {
                    $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                    return $this->jsonResponseWithErrors("The Provided Organization Type does not Exists!");
                }

            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function updateOrganizationById(Request $request){
        try{

            if( empty($request->input('id')) ){
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Organization ID must be provided!");
            }
            $validator = Validator::make($request->all(), [
                'name' => ["max:100",
                    Rule::unique('organizations')->ignore($request->input('id'))],
                'registration_id' => ["string",
                    "max:20",
                    Rule::unique('organizations')->ignore($request->input('id'))],
                'uan' => ["digits_between:1,10",
                    Rule::unique('organizations')->ignore($request->input('id'))],
                'ntn' => ["string",
                    "max:20",
                    Rule::unique('organizations')->ignore($request->input('id'))],
                'stn' => ["string",
                    "max:20",
                    Rule::unique('organizations')->ignore($request->input('id'))],
                'verified' => 'required|boolean'
            ]);

            $organization_id = $request->input('id');

            if($validator->fails()) {

                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);

            }else{

                $organization = Organization::where('id', $organization_id)->with('type')->first();
                if(!empty($organization)){

                    //organization update
                    if( !empty($request->input('name')) ) {
                        $organization->name = $request->input('name');
                    }
                    if( !empty($request->input('registration_id')) ) {
                        $organization->registration_id = $request->input('registration_id');
                    }
                    if( !empty($request->input('uan')) ) {
                        $organization->uan = $request->input('uan');
                    }
                    if( !empty($request->input('ntn')) ) {
                        $organization->ntn = $request->input('ntn');
                    }
                    if( !empty($request->input('stn')) ) {
                        $organization->stn = $request->input('stn');
                    }
                    if( !empty($request->input('organization_type_id')) ) {
                        $organization->organization_type_id = $request->input('organization_type_id');
                    }

                    $organization->verified = $request->input('verified');

                    $organization->updated_at = Carbon::now();
                    $organization->save();
                    $organization = Organization::where('id', $organization_id)->with('type')->first();

                    // type and locations
                    return (new OrganizationResource($organization,JsonResponse::HTTP_OK,"Organizations Updated!", true, false))
                        ->response()->setStatusCode(JsonResponse::HTTP_OK);

                } else {
                    $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                    return $this->jsonResponseWithErrors("The Organization does not Exists!");
                }
            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function getOrganizationById($id){
        try{
            if($id != '') {
                $organization = Organization::where('id',$id)->with(['type', 'locations'])->first();

                if( !empty($organization) ){

                    //type and locations
                    return (new OrganizationResource($organization,JsonResponse::HTTP_OK,"Organization Found!", true, true))
                        ->response()->setStatusCode(JsonResponse::HTTP_OK);

                } else {
                    $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                    return $this->jsonResponseWithErrors("Organization does not Exists!");
                }

            } else {
                $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                return $this->jsonResponseWithErrors("Organization ID must be provided!");
            }
        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    /*
     *  Organizations Types
     */
    public function deleteOrganizationType($id){
        try{

            $type = OrganizationType::where('id', $id)->first();
            if(!empty($type)){

                $type->delete($id);

                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->jsonResponseWithMessage("Organization Type Deleted: ".$type->name);

            } else {
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Organization Type does not Exists!");
            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function getAllOrganizationType(){
        try{

            $orgnizationtypes = OrganizationType::all();

            if( !empty($orgnizationtypes) ){
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->customReposneWithStatusAndMessage($orgnizationtypes, 'Organization\'s Types Found!');
            } else {
                $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                return $this->jsonResponseWithErrors("No Organization's Type Found!");
            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function addNewOrganizationType(Request $request){
        try{

            $validator = Validator::make($request->all(), [
                'name' => 'required|max:50|unique:organization_types',
                'public' => 'required|boolean'
            ]);

            if($validator->fails()) {

                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);

            }else{

                $type = new OrganizationType;
                $type->name = $request->input('name');
                $type->public = $request->input('public');

                $type->created_at = Carbon::now();
                $type->updated_at = Carbon::now();

                $type->save();

                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->customReposneWithStatusAndMessage($type, 'Type Added!');

            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function updateOrganizationType(Request $request){
        try{

            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'name' => ["max:50",Rule::unique('organization_types')->ignore($request->input('id'))],
                'public' => 'required|boolean'
            ]);

            $type_id = $request->input('id');

            if($validator->fails()) {

                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);

            }else{

                $type = OrganizationType::where('id', $type_id)->first();
                if(!empty($type)){

                    //organization type update
                    if( !empty($request->input('name')) ) {
                        $type->name = $request->input('name');
                    }

                    $type->public = $request->input('public');

                    $type->updated_at = Carbon::now();
                    $type->save();

                    $this->setStatusCode(JsonResponse::HTTP_OK);
                    return $this->customReposneWithStatusAndMessage($type,'Orgnization Type Updated');

                } else {
                    $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                    return $this->jsonResponseWithErrors("The Organization Type does not Exists!");
                }
            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function getOrganizationTypeById($id){
        try{
            if($id != '') {
                $type = OrganizationType::where('id',$id)->first();

                if( !empty($type) ){
                    $this->setStatusCode(JsonResponse::HTTP_OK);
                    return $this->customReposneWithStatusAndMessage($type, "Organization's Type Found!");
                } else {
                    $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                    return $this->jsonResponseWithErrors("Organization's Type does not Exists!");
                }

            } else {
                $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                return $this->jsonResponseWithErrors("Organization's Type ID must be provided!");
            }
        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    /*
     *  Organizations Locations
     */
    public function deleteOrganizationLocation($id){
        try{

            $location = OrganizationLocation::where('id', $id)->first();
            if(!empty($location)){

                $location->delete($id);

                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->jsonResponseWithMessage("Organization's Location Deleted: ".$location->name);

            } else {
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Provided Organization's Location does not Exists!");
            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function getAllOrganizationLocations(){
        try{

            $locations = OrganizationLocation::with(['organization','city'])->get();

            if( !empty($locations) ){
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->customReposneWithStatusAndMessage($locations, 'Organization\'s Locations Found!');
            } else {
                $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                return $this->jsonResponseWithErrors("No Organization's Location Found!");
            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function addNewOrganizationLocation(Request $request){
        try{

            $enum = $this->getEnumValues("organization_locations", "type");

            $validator = Validator::make($request->all(), [
                'name' => 'required|max:50',
                'organization_id' => 'required',
                'city_id' => 'required',
                'organization_location_uan' => 'digits_between:1,10|unique:organization_locations,organization_location_uan',
                'organization_telephone_1' => 'required|digits_between:1,10|unique:organization_locations,organization_telephone_1',
                'organization_telephone_2' => 'digits_between:1,10|unique:organization_locations,organization_telephone_2',
                'organization_telephone_3' => 'digits_between:1,10|unique:organization_locations,organization_telephone_3',
                'organization_fax_1' => 'digits_between:1,10|unique:organization_locations,organization_fax_1',
                'organization_fax_2' => 'digits_between:1,10|unique:organization_locations,organization_fax_2',
                'email' => 'required|email',
                'address' => 'required|string|max:100',
                'building' => 'required|string|max:100',
                'street' => 'required|string|max:50',
                'type' => ['required',Rule::in($enum)],
                'primary' => 'required|boolean',
                'verified' => 'required|boolean'
            ]);

            $organization_id = $request->input('organization_id');
            $city_id = $request->input('city_id');

            if($validator->fails()) {

                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);

            }else{

                $organization = Organization::where('id',$organization_id)->first();
                $city = City::where('id',$city_id)->first();

                if( !empty($organization) ) {
                    if( !empty($city) ) {

                        $location = new OrganizationLocation;
                        $location->name = $request->input('name');
                        $location->organization_id = $organization_id;
                        $location->city_id = $city_id;
                        $location->organization_telephone_1 = $request->input('organization_telephone_1');
                        $location->email = $request->input('email');
                        $location->address = $request->input('address');
                        $location->building = $request->input('building');
                        $location->street = $request->input('street');
                        $location->type = $request->input('type');
                        $location->primary = $request->input('primary');
                        $location->verified = $request->input('verified');

                        if (!empty($request->input('organization_location_uan'))) {
                            $location->organization_location_uan = $request->input('organization_location_uan');
                        }
                        if (!empty($request->input('organization_telephone_2'))) {
                            $location->organization_telephone_2 = $request->input('organization_telephone_2');
                        }
                        if (!empty($request->input('organization_telephone_3'))) {
                            $location->organization_telephone_3 = $request->input('organization_telephone_3');
                        }
                        if (!empty($request->input('organization_fax_1'))) {
                            $location->organization_fax_1 = $request->input('organization_fax_1');
                        }
                        if (!empty($request->input('organization_fax_2'))) {
                            $location->organization_fax_2 = $request->input('organization_fax_2');
                        }

                        $location->created_at = Carbon::now();
                        $location->updated_at = Carbon::now();

                        $location->save();

                        $new_location = OrganizationLocation::where('id', $location->id)->with('city')->first();

                        $this->setStatusCode(JsonResponse::HTTP_OK);
                        return $this->customReposneWithStatusAndMessage($new_location, 'Organization Location Added!');

                    } else {
                        $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                        return $this->jsonResponseWithErrors("The Provided City does not Exists!");
                    }

                } else {
                    $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                    return $this->jsonResponseWithErrors("The Provided Organization does not Exists!");
                }

            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function updateOrganizationLocation(Request $request){
        try{

            $enum = $this->getEnumValues("organization_locations", "type");

            if( empty($request->input('id')) ){
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Organization's Location ID must be provided!");
            }
            $validator = Validator::make($request->all(), [
                'name' => 'max:50',
                'organization_location_uan' => ['digits_between:1,10',
                    Rule::unique('organization_locations')->ignore($request->input('id'))],
                'organization_telephone_1' => ['digits_between:1,10',
                    Rule::unique('organization_locations')->ignore($request->input('id'))],
                'organization_telephone_2' => ['digits_between:1,10',
                    Rule::unique('organization_locations')->ignore($request->input('id'))],
                'organization_telephone_3' => ['digits_between:1,10',
                    Rule::unique('organization_locations')->ignore($request->input('id'))],
                'organization_fax_1' => ['digits_between:1,10',
                    Rule::unique('organization_locations')->ignore($request->input('id'))],
                'organization_fax_2' => ['digits_between:1,10',
                    Rule::unique('organization_locations')->ignore($request->input('id'))],
                'email' => 'email',
                'address' => 'string|max:100',
                'building' => 'string|max:100',
                'street' => 'string|max:50',
                'type' => [Rule::in($enum)],
                'primary' => 'required|boolean',
                'verified' => 'required|boolean'
            ]);

            if($validator->fails()) {

                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);

            }else{

                $location = OrganizationLocation::where('id', $request->input('id'))->with('city')->first();
                if(!empty($location)){

                    //organization's location update
                    if( !empty($request->input('name')) ) {
                        $location->name = $request->input('name');
                    }
                    if( !empty($request->input('organization_location_uan')) ) {
                        $location->organization_location_uan = $request->input('organization_location_uan');
                    }
                    if( !empty($request->input('organization_telephone_1')) ) {
                        $location->organization_telephone_1 = $request->input('organization_telephone_1');
                    }
                    if( !empty($request->input('organization_telephone_2')) ) {
                        $location->organization_telephone_2 = $request->input('organization_telephone_2');
                    }
                    if( !empty($request->input('organization_telephone_3')) ) {
                        $location->organization_telephone_3 = $request->input('organization_telephone_3');
                    }
                    if( !empty($request->input('organization_fax_1')) ) {
                        $location->organization_fax_1 = $request->input('organization_fax_1');
                    }
                    if( !empty($request->input('organization_fax_2')) ) {
                        $location->organization_fax_2 = $request->input('organization_fax_2');
                    }
                    if( !empty($request->input('email')) ) {
                        $location->email = $request->input('email');
                    }
                    if( !empty($request->input('address')) ) {
                        $location->address = $request->input('address');
                    }
                    if( !empty($request->input('building')) ) {
                        $location->building = $request->input('building');
                    }
                    if( !empty($request->input('street')) ) {
                        $location->street = $request->input('street');
                    }
                    if( !empty($request->input('type')) ) {
                        $location->type = $request->input('type');
                    }

                    $location->primary = $request->input('primary');
                    $location->verified = $request->input('verified');

                    $location->updated_at = Carbon::now();
                    $location->save();

                    $this->setStatusCode(JsonResponse::HTTP_OK);
                    return $this->customReposneWithStatusAndMessage($location, "Organization's Location Updated!");

                } else {
                    $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                    return $this->jsonResponseWithErrors("The Organization's Location does not Exists!");
                }
            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function getOrganizationLocationById($id){
        try{
            if($id != '') {
                $location = OrganizationLocation::where('id',$id)->with(['organization','city'])->first();

                if( !empty($location) ){
                    $this->setStatusCode(JsonResponse::HTTP_OK);
                    return $this->customReposneWithStatusAndMessage($location, 'Organization\'s Location Found!');
                } else {
                    $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                    return $this->jsonResponseWithErrors("Organization's Location does not Exists!");
                }

            } else {
                $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                return $this->jsonResponseWithErrors("Organization's Location ID must be provided!");
            }
        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    /*
     *  Organizations Location's Contact Person
     */
    public function deleteContactPerson($id){
        try{

            $person = LocationContactPerson::where('id', $id)->first();
            if(!empty($person)){

                $person->delete($id);

                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->jsonResponseWithMessage("Location's Person Deleted: ".$person->name);

            } else {
                $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                return $this->jsonResponseWithErrors("The Provided Location's Person does not Exists!");
            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function getAllContactPersons(){
        try{

            $persons = LocationContactPerson::with(['organization','organization_location'])->get();

            if( !empty($persons) ){
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->customReposneWithStatusAndMessage($persons, 'Contact Person Found!');
            } else {
                $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                return $this->jsonResponseWithErrors("No Contact Person Found!");
            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function addNewContactPerson(Request $request){
        try{

            $validator = Validator::make($request->all(), [
                'organization_id' => 'required',
                'organization_location_id' => 'required',
                'name' => 'required|string|max:50',
                'designation' => 'required|string|max:50',
                'mobile' => 'required|digits_between:1,10',
                'email' => 'required|email',
                'priority' => 'required|digits_between:1,10',
                'verified' => 'required|boolean',
                'user' => 'required|boolean'
            ]);

            $organization_id = $request->input('organization_id');
            $organization_location_id = $request->input('organization_location_id');

            if($validator->fails()) {

                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);

            }else{

                $organization = Organization::where('id',$organization_id)->first();
                $location = OrganizationLocation::where('id',$organization_location_id)->first();

                if( !empty($organization) ) {
                    if( !empty($location) ) {

                        $person = new LocationContactPerson();
                        $person->organization_id = $organization_id;
                        $person->organization_location_id = $organization_location_id;
                        $person->name = $request->input('name');
                        $person->designation = $request->input('designation');
                        $person->mobile = $request->input('mobile');
                        $person->email = $request->input('email');
                        $person->priority = $request->input('priority');
                        $person->verified = $request->input('verified');
                        $person->user = $request->input('user');

                        $person->created_at = Carbon::now();
                        $person->updated_at = Carbon::now();

                        $person->save();

                        $new_person = LocationContactPerson::where('id', $person->id)->with(['organization','organization_location'])->first();

                        $this->setStatusCode(JsonResponse::HTTP_OK);
                        return $this->customReposneWithStatusAndMessage($new_person, 'Location Person Added!');

                    } else {
                        $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                        return $this->jsonResponseWithErrors("The Provided Organization's Location does not Exists!");
                    }

                } else {
                    $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                    return $this->jsonResponseWithErrors("The Provided Organization does not Exists!");
                }

            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function updateContactPerson(Request $request){
        try{

            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'name' => 'string|max:50',
                'designation' => 'string|max:50',
                'mobile' => 'digits_between:1,10',
                'email' => 'email',
                'priority' => 'digits_between:1,10',
                'verified' => 'required|boolean',
                'user' => 'required|boolean'
            ]);

            $person_id = $request->input('id');

            if($validator->fails()) {

                $errors = $validator->errors()->getMessages();
                $errors_messages = [];
                foreach ($errors as $name => $error) {
                    $errors_messages[$name] = $error[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_messages);

            }else{

                $person = LocationContactPerson::where('id', $person_id)->first();
                if(!empty($person)){

                    //location's contact person update
                    if( !empty($request->input('name')) ) {
                        $person->name = $request->input('name');
                    }
                    if( !empty($request->input('designation')) ) {
                        $person->designation = $request->input('designation');
                    }
                    if( !empty($request->input('mobile')) ) {
                        $person->mobile = $request->input('mobile');
                    }
                    if( !empty($request->input('email')) ) {
                        $person->email = $request->input('email');
                    }
                    if( !empty($request->input('priority')) ) {
                        $person->priority = $request->input('priority');
                    }

                    $person->verified = $request->input('verified');
                    $person->user = $request->input('user');

                    $person->updated_at = Carbon::now();
                    $person->save();

                    $new_person = LocationContactPerson::where('id', $person->id)->with(['organization','organization_location'])->first();

                    $this->setStatusCode(JsonResponse::HTTP_OK);
                    return $this->customReposneWithStatusAndMessage($new_person, "Location's Contact Person Updated!");

                } else {
                    $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
                    return $this->jsonResponseWithErrors("The Provided Location's Contact Person does not Exists!");
                }
            }

        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function getContactPersonById($id){
        try{
            if($id != '') {
                $person = LocationContactPerson::where('id',$id)->with(['organization','organization_location'])->first();

                if( !empty($person) ){
                    $this->setStatusCode(JsonResponse::HTTP_OK);
                    return $this->customReposneWithStatusAndMessage($person, 'Contact Person Found!');
                } else {
                    $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                    return $this->jsonResponseWithErrors("Contact Person does not Exists!");
                }

            } else {
                $this->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                return $this->jsonResponseWithErrors("Location's Contact Person ID must be provided!");
            }
        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

}
