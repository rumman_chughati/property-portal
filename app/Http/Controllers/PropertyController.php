<?php

namespace App\Http\Controllers;

use App\Http\ObjectTransformation;
use App\Http\Resources\PropertyListingCollection;
use App\Http\Resources\PropertyListingResource;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Property;
use Validator;

class PropertyController extends Controller
{
    use ObjectTransformation;

    /*
     * Properties
     */
    public function deleteProperty($id){
        if($id !=''){
            $count = Property::where('id',$id)->count();
            if($count > 0){
                $property = Property::where('id',$id);
                $property->delete();
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->jsonResponseWithMessage("Propery Deleted Successfully");
            }else{
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors("Property Not Found");
            }
        }else{
            $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            return $this->jsonResponseWithErrors("Property Id not Provided");
        }
    }

    public function getAllPropertyListing(){
        try{
            $propertyListing = Property::with('user')->with('approvedByUser')->get();
            return (new PropertyListingCollection($propertyListing,JsonResponse::HTTP_OK,"Property Listing Found"))
                ->response()->setStatusCode(JsonResponse::HTTP_OK);
        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function updatePropertyListing(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'amount' => 'sometimes',
                'land_area' => 'required',
                'ownership' => 'required',
                'bidding_type' => 'required',
                'property_classification' => 'required',
                'property_purpose' => 'required',
            ]);
            if ($validator->fails()) {
                $errors = $validator->errors()->getMessages();
                $errors_response = [];
                foreach ($errors as $name => $errors_array) {
                    $errors_response[$name] = $errors_array[0];
                }
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors($errors_response);
            }else{
                $property_id = $request->input('id');
                $count = Property::where('id',$property_id)->count();
                if($count > 0){
                    $property = Property::where('id',$property_id)->update($request->all());
                    if($property > 0){
                        $this->setStatusCode(JsonResponse::HTTP_OK);
                        return $this->jsonResponseWithMessage('Property Listing Updated Successfully');
                    }else{
                        $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                        return $this->jsonResponseWithErrors("Property Not Update Successfully");
                    }
                }else{
                    $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                    return $this->jsonResponseWithErrors("Property Not Found");
                }
            }
        }catch(\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            return $this->jsonResponseWithErrors("Property Id not Provided");
        }
    }

    public function getPropertyListingById($id){
        try{
            if($id){
                $propertyListing =  Property::where('id',$id)->with('user')->with('approvedByUser')->first();

                return (new PropertyListingResource($propertyListing,JsonResponse::HTTP_OK,"Property Listing Found"))
                    ->response()->setStatusCode(JsonResponse::HTTP_OK);
            }else{
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors("Property Id Not Provided");
            }

        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

    public function changePropertyStatusById($id, $status){
        if($id != ''){
            $count = Property::where('id',$id)->count();
            if($count > 0){
                $property = Property::where('id',$id)->first();
                $property->approved_status = $status;
                if($status == 'rejected'){
                    $property->rejected_date = Carbon::now();
                }
                if($status == 'approved'){
                    $property->approved_date = Carbon::now();
                }
                $property->save();
                $this->setStatusCode(JsonResponse::HTTP_OK);
                return $this->jsonResponseWithMessage("Account Status Changed");
            }else{
                $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
                return $this->jsonResponseWithErrors("Property Not Found");
            }
        }else{
            $this->setStatusCode(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            return $this->jsonResponseWithErrors("Property Id not Provided");
        }
    }

    public function getAllPropertyCount(){
        try{
            $propertyListing = Property::all()->count();
            $this->setStatusCode(JsonResponse::HTTP_OK);
            return $this->customReposneWithStatusAndMessage($propertyListing, 'Property Count!');
        }catch (\Exception $e){
            $this->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->jsonResponseWithErrors($e->getMessage());
        }
    }

}
