<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scheme extends Model
{

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at',
    ];

    public function city(){
        return $this->belongsTo(City::class);
    }

    public function phases(){
        return $this->hasMany(Phase::class);
    }
}
