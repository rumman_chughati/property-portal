<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrganizationLocation extends Model
{
    public function organization(){
        return $this->belongsTo(Organization::class, 'organization_id', 'id');
    }

    public function city(){
        return $this->belongsTo(City::class, 'city_id', 'id');
    }

    public function contact_persons(){
        return $this->hasMany(LocationContactPerson::class);
    }

}
