<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    public function type(){
        return $this->belongsTo(OrganizationType::class, 'organization_type_id', 'id');
    }

    public function locations(){
        return $this->hasMany(OrganizationLocation::class);
    }

    public function contact_persons(){
        return $this->hasMany(LocationContactPerson::class);
    }
}
