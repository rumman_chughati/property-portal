<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Portal extends Model
{
    protected $hidden = [
        'updated_at', 'created_at', 'pivot', 'deleted_at'
    ];

    public function uom(){
        return $this->belongsToMany(Uom::class);
    }

    public function prodcat(){
        return $this->belongsToMany(Prodcat::class);
    }

}
