<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public function region(){
        return $this->belongsTo(Region::class,'region_id','id');
    }

    public function states(){
        return $this->hasMany(State::class);
    }

}
