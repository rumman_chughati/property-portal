<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'auth:api'], function () {

    Route::post('logout', 'UsersController@logout');
    Route::post('me', 'UsersController@me');

    Route::group(['middleware' => 'role:superAdmin'], function () {

        //Roles
        Route::delete('Admin/Role', 'UsersController@removeUserRole');
        Route::get('Admin/Role', 'UsersController@getAllRoles');
        Route::put('Admin/Role', 'UsersController@setUserRole');
        Route::get('Admin/Role/{user_id}', 'UsersController@getUserRole');

        //Permissions
        Route::get('Admin/Permission', 'UsersController@getAllPermissions');
        Route::put('Admin/Permission/{role_id}','UsersController@updateRolePermissions');
        Route::get('Admin/Permission/{role}', 'UsersController@getRolePermissions');

        //Users
        Route::delete('Admin/User/{id}','UsersController@deleteUserById');
        Route::get('Admin/User','UsersController@getAllUsers');
        Route::put('Admin/User','UsersController@updateUserById');
        Route::get('Admin/User/{id}','UsersController@getUserById');
        Route::get('Admin/User/status/{id}/{status}','UsersController@changeUserStatusById');

        //Property Listing
        Route::delete('Admin/Property/{id}','PropertyController@deleteProperty');
        Route::get('Admin/Property','PropertyController@getAllPropertyListing');
        Route::put('Admin/Property','PropertyController@updatePropertyListing');
        Route::get('Admin/Property/{id}','PropertyController@getPropertyListingById');
        Route::get('Admin/Property/status/{id}/{status}','PropertyController@changePropertyStatusById');
        //Dashboard Calls
        Route::get('Admin/Property/count/total','PropertyController@getAllPropertyCount');
        Route::get('Admin/User/count/total','UsersController@getAllUserCount');

        //Property Features
        Route::delete('Admin/PropertyFeature/{id}','FeaturesController@deleteFeatureById');
        Route::get('Admin/PropertyFeature','FeaturesController@getAllPropertyFeatures');
        Route::post('Admin/PropertyFeature','FeaturesController@addNewFeature');
        Route::put('Admin/PropertyFeature','FeaturesController@updateFeatureById');
        Route::get('Admin/PropertyFeature/{id}','FeaturesController@getPropertyFeatureById');

        //Property Sub-Features
        Route::delete('Admin/PropertySubFeature/{id}','FeaturesController@deleteSubFeatureById');
        Route::post('Admin/PropertySubFeature','FeaturesController@addNewSubFeature');
        Route::put('Admin/PropertySubFeature','FeaturesController@updateSubFeatureById');

        //Property Class
        Route::delete('Admin/PropertyClass/{id}','TypesController@deletePropertyClassById');
        Route::get('Admin/PropertyClass','TypesController@getAllPropertyClasses');
        Route::post('Admin/PropertyClass','TypesController@addNewPropertyClass');
        Route::put('Admin/PropertyClass','TypesController@updatePropertyClassById');
        Route::get('Admin/PropertyClass/{id}','TypesController@getPropertyClassById');

        //Property Types
        Route::delete('Admin/PropertyType/{id}','TypesController@deletePropertyTypeById');
        Route::post('Admin/PropertyType','TypesController@addNewPropertyType');
        Route::put('Admin/PropertyType','TypesController@updatePropertyTypeById');

        //Regions
        Route::delete('Admin/Region/{id}','RegionsController@deleteRegionById');
        Route::get('Admin/Region','RegionsController@getAllRegions');
        Route::post('Admin/Region','RegionsController@addNewRegion');
        Route::put('Admin/Region','RegionsController@updateRegionById');
        Route::get('Admin/Region/{id}','RegionsController@getRegionById');

        //Countries
        Route::delete('Admin/Country/{id}','RegionsController@deleteCountryById');
        Route::get('Admin/Country','RegionsController@getAllCountries');
        Route::post('Admin/Country','RegionsController@addNewCountry');
        Route::put('Admin/Country','RegionsController@updateCountryById');
        Route::get('Admin/Country/{id}','RegionsController@getCountryById');

        //States
        Route::delete('Admin/State/{id}','RegionsController@deleteStateById');
        Route::get('Admin/State','RegionsController@getAllStates');
        Route::post('Admin/State','RegionsController@addNewState');
        Route::put('Admin/State','RegionsController@updateStateById');
        Route::get('Admin/State/{id}','RegionsController@getStateById');

        //Cities
        Route::delete('Admin/City/{id}','RegionsController@deleteCityById');
        Route::get('Admin/City','RegionsController@getAllCities');
        Route::post('Admin/City','RegionsController@addNewCity');
        Route::put('Admin/City','RegionsController@updateCityById');
        Route::get('Admin/City/{id}','RegionsController@getCityById');

        //schemes
        Route::delete('Admin/Scheme/{id}','RegionsController@deleteSchemeById');
        Route::get('Admin/Scheme','RegionsController@getAllSchemes');
        Route::post('Admin/Scheme','RegionsController@addNewScheme');
        Route::put('Admin/Scheme','RegionsController@updateSchemeById');
        Route::get('Admin/Scheme/{id}','RegionsController@getSchemeById');

        //phases
        Route::delete('Admin/Phase/{id}','RegionsController@deletePhaseById');
        Route::get('Admin/Phase','RegionsController@getAllPhases');
        Route::post('Admin/Phase','RegionsController@addNewPhase');
        Route::put('Admin/Phase','RegionsController@updatePhaseById');
        Route::get('Admin/Phase/{id}','RegionsController@getPhaseById');

        //blocks
        Route::delete('Admin/Block/{id}','RegionsController@deleteBlockById');
        Route::get('Admin/Block','RegionsController@getAllBlocks');
        Route::post('Admin/Block','RegionsController@addNewBlock');
        Route::put('Admin/Block','RegionsController@updateBlockById');
        Route::get('Admin/Block/{id}','RegionsController@getBlockById');

        //Organizations
        Route::delete('Admin/Organization/{id}','OrganizationsController@deleteOrganizationById');
        Route::get('Admin/Organization','OrganizationsController@getAllOrganizations');
        Route::post('Admin/Organization','OrganizationsController@addNewOrganization');
        Route::put('Admin/Organization','OrganizationsController@updateOrganizationById');
        Route::get('Admin/Organization/{id}','OrganizationsController@getOrganizationById');

        //Organization Types
        Route::delete('Admin/OrganizationType/{id}','OrganizationsController@deleteOrganizationType');
        Route::get('Admin/OrganizationType','OrganizationsController@getAllOrganizationType');
        Route::post('Admin/OrganizationType','OrganizationsController@addNewOrganizationType');
        Route::put('Admin/OrganizationType','OrganizationsController@updateOrganizationType');
        Route::get('Admin/OrganizationType/{id}','OrganizationsController@getOrganizationTypeById');

        //Organization Locations
        Route::delete('Admin/OrganizationLocation/{id}','OrganizationsController@deleteOrganizationLocation');
        Route::get('Admin/OrganizationLocation','OrganizationsController@getAllOrganizationLocations');
        Route::post('Admin/OrganizationLocation','OrganizationsController@addNewOrganizationLocation');
        Route::put('Admin/OrganizationLocation','OrganizationsController@updateOrganizationLocation');
        Route::get('Admin/OrganizationLocation/{id}','OrganizationsController@getOrganizationLocationById');

        //Organization Location's Contact Person
        Route::delete('Admin/ContactPerson/{id}','OrganizationsController@deleteContactPerson');
        Route::get('Admin/ContactPerson','OrganizationsController@getAllContactPersons');
        Route::post('Admin/ContactPerson','OrganizationsController@addNewContactPerson');
        Route::put('Admin/ContactPerson','OrganizationsController@updateContactPerson');
        Route::get('Admin/ContactPerson/{id}','OrganizationsController@getContactPersonById');

        //portal
        Route::delete('Admin/Portal/{id}','PortalsController@deletePortal');
        Route::get('Admin/Portal','PortalsController@getAllPortals');
        Route::post('Admin/Portal','PortalsController@addNewPortal');
        Route::put('Admin/Portal','PortalsController@updatePortal');
        Route::get('Admin/Portal/{id}','PortalsController@getPortalById');

        //Unit of measure (uom)
        Route::delete('Admin/UnitOfMeasurement/{id}','PortalsController@deleteUnitOfMeasurement');
        Route::get('Admin/UnitOfMeasurement','PortalsController@getAllUnitOfMeasurement');
        Route::post('Admin/UnitOfMeasurement','PortalsController@addNewUnitOfMeasurement');
        Route::put('Admin/UnitOfMeasurement','PortalsController@updateUnitOfMeasurement');
        Route::get('Admin/UnitOfMeasurement/{id}','PortalsController@getUnitOfMeasurementById');

        //unit of measure conversion
        Route::delete('Admin/UnitOfMeasurementConversion/{id}','PortalsController@deleteUnitOfMeasurementConversion');
        Route::get('Admin/UnitOfMeasurementConversion','PortalsController@getAllUnitOfMeasurementConversion');
        Route::post('Admin/UnitOfMeasurementConversion','PortalsController@addNewUnitOfMeasurementConversion');
        Route::put('Admin/UnitOfMeasurementConversion','PortalsController@updateUnitOfMeasurementConversion');
        Route::get('Admin/UnitOfMeasurementConversion/{id}','PortalsController@getUnitOfMeasurementConversionById');

        //deal type
        Route::delete('Admin/DealType/{id}','TypesController@deleteDealType');
        Route::get('Admin/DealType','TypesController@getAllDealType');
        Route::post('Admin/DealType','TypesController@addNewDealType');
        Route::put('Admin/DealType','TypesController@updateDealType');
        Route::get('Admin/DealType/{id}','TypesController@getDealTypeById');

        //products and category
        Route::delete('Admin/ProductCategory/{id}','ProductCategoryController@deleteProductCategory');
        Route::get('Admin/ProductCategory','ProductCategoryController@getAllProductCategory');
        Route::post('Admin/ProductCategory','ProductCategoryController@addNewProductCategory');
        Route::put('Admin/ProductCategory','ProductCategoryController@updateProductCategory');
        Route::get('Admin/ProductCategory/{id}','ProductCategoryController@getProductCategoryById');
        Route::post('Admin/LinkPortalProdcat','ProductCategoryController@linkProdcatWithPortal');

        //attributes
        Route::delete('Admin/Attribute/{id}','ProductCategoryController@deleteAttribute');
        Route::get('Admin/Attribute','ProductCategoryController@getAllAttributes');
        Route::post('Admin/Attribute','ProductCategoryController@addNewAttribute');
        Route::put('Admin/Attribute','ProductCategoryController@updateAttribute');
        Route::get('Admin/Attribute/{id}','ProductCategoryController@getAttributeById');
        Route::post('Admin/LinkAttributeProdcat','ProductCategoryController@linkAttributeWithProdcat');

        //product category and attributes
        Route::delete('Admin/ProdcatAttribute/{id}','ProductCategoryController@deleteProdcatAttribute');
        Route::get('Admin/ProdcatAttribute','ProductCategoryController@getAllProdcatAttributes');
        Route::post('Admin/ProdcatAttribute','ProductCategoryController@addNewProdcatAttribute');
        Route::put('Admin/ProdcatAttribute','ProductCategoryController@updateProdcatAttribute');
        Route::get('Admin/ProdcatAttribute/{id}','ProductCategoryController@getProdcatAttributeById');

        //prodcat tree calls
        Route::put('Admin/ProdcatTree','ProductCategoryController@updateProdcatTree');
        Route::get('Admin/ProdcatTree/{id}','ProductCategoryController@getProdcatByPortalId');

    });

});

Route::post('register', 'UsersController@register');
Route::post('login', 'UsersController@login');
Route::post('updatePassword', 'UsersController@updatePassword');
Route::get('verification/{email}/{token}', 'UsersController@verification');
//Route::get('check/{parentid}','ProductCategoryController@recursiveProdcatDelete');