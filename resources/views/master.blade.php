<!doctype html>
<html lang="en"  dir="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">

    <title>Material Trading Portal</title>
    <link rel="icon" type="image/png" href="{{ asset('favicon.ico') }}">
    <!-- fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i">
    <!-- css -->
    <link rel="stylesheet" href="{{ asset('site/vendor/bootstrap-4.2.1/css/bootstrap-grid.min.css') }}">
    <link rel="stylesheet" href="{{ asset('site/vendor/bootstrap-4.2.1/css/bootstrap-reboot.min.css') }}">
    <link rel="stylesheet" href="{{ asset('site/vendor/bootstrap-4.2.1/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('site/css/style.css') }}">

    <script src="{{ asset('site/vendor/jquery-3.3.1/jquery.min.js') }}"></script>
    <script src="{{ asset('site/vendor/bootstrap-4.2.1/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('site/vendor/owl-carousel-2.3.4/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('site/vendor/nouislider-12.1.0/nouislider.min.js') }}"></script>
    <script src="{{ asset('site/js/number.js') }}"></script>
    <script src="{{ asset('site/js/main.js') }}"></script>
    <script src="{{ asset('site/vendor/svg4everybody-2.1.9/svg4everybody.min.js') }}"></script>
    <script>svg4everybody();</script>
    <!-- font - fontawesome -->
    <link rel="stylesheet" href="{{ asset('site/vendor/fontawesome-5.6.1/css/all.min.css') }}">
    <!-- font - stroyka -->
    <link rel="stylesheet" href="{{asset('site/fonts/stroyka/stroyka.css')}}">
</head>
<body>
    <div id="quickview-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content"></div>
        </div>
    </div>
    @include('layouts/mobile_menu')
    <div class="site">
        @include('layouts/header')
        <div class="site__body">
            @yield('content')
            @include('layouts/footer')
        </div>
    </div>
    <!-- js -->

</body>
</html>