<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('region_id');
            $table->foreign('region_id')
                ->references('id')->on('regions')
                ->onDelete('cascade');

            $table->string('country_code',2)->unique();
            $table->string('name', 100)->unique();
            $table->unsignedInteger('country_idd')->unique();
            $table->string('country_iso3', 3)->unique();
            $table->boolean('public', false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
