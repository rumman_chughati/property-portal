<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdcatAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prodcat_attributes', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('attribute_id');
            $table->foreign('attribute_id')
                ->references('id')->on('attributes')
                ->onDelete('cascade');

            $table->unsignedInteger('prodcat_id');
            $table->foreign('prodcat_id')
                ->references('id')->on('prodcats')
                ->onDelete('cascade');

            $table->string('prodcat_attribute_val',50);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prodcat_attributes');
    }
}
