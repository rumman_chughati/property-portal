<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organization_locations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);

            $table->unsignedInteger('organization_id');
            $table->foreign('organization_id')
                ->references('id')->on('organizations')
                ->onDelete('cascade');

            $table->unsignedInteger('city_id');
            $table->foreign('city_id')
                ->references('id')->on('cities')
                ->onDelete('cascade');

            $table->unsignedInteger("organization_location_uan")->nullable();
            $table->unsignedInteger("organization_telephone_1");
            $table->unsignedInteger("organization_telephone_2")->nullable();
            $table->unsignedInteger("organization_telephone_3")->nullable();
            $table->unsignedInteger("organization_fax_1")->nullable();
            $table->unsignedInteger("organization_fax_2")->nullable();
            $table->string("email", 50);
            $table->string("address", 100);
            $table->string("building", 100);
            $table->string("street", 50);
            $table->enum('type',['type1','type2']);
            $table->boolean('primary', false);
            $table->boolean('verified', false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organization_locations');
    }
}
