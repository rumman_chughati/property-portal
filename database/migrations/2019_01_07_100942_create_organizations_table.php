<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);

            $table->unsignedInteger('organization_type_id');
            $table->foreign('organization_type_id')
                ->references('id')->on('organization_types')
                ->onDelete('cascade');

            $table->string("registration_id", 20);
            $table->unsignedInteger("uan")->unique()->nullable();
            $table->string("ntn", 20)->unique();
            $table->string("stn", 20)->unique();
            $table->boolean('verified', false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations');
    }
}
