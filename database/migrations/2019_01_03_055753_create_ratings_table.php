<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ratings', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('from_user');
            $table->foreign('from_user')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table->unsignedInteger('to_user');
            $table->foreign('to_user')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table->dateTime('rating_date')->nullable();
            $table->unsignedInteger('rating')->default(0);
            $table->string('remarks')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ratings');
    }
}
