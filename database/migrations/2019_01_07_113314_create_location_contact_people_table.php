<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationContactPeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('location_contact_people', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('organization_id');
            $table->foreign('organization_id')
                ->references('id')->on('organizations')
                ->onDelete('cascade');

            $table->unsignedInteger('organization_location_id');
            $table->foreign('organization_location_id')
                ->references('id')->on('organization_locations')
                ->onDelete('cascade');

            $table->string("name", 50);
            $table->string("designation", 50);
            $table->unsignedInteger("mobile");
            $table->string("email", 50);
            $table->unsignedInteger("priority");
            $table->boolean('verified', false);
            $table->boolean('user', false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('location_contact_people');
    }
}
