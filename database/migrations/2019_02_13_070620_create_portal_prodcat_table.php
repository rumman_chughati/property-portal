<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortalProdcatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portal_prodcat', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('prodcat_id');
            $table->foreign('prodcat_id')
                ->references('id')->on('prodcats')
                ->onDelete('cascade');

            $table->unsignedInteger('portal_id');
            $table->foreign('portal_id')
                ->references('id')->on('portals')
                ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
//
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portal_prodcat');
    }
}
