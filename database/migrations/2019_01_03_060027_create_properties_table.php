<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table->enum('sold_status',['sold','onSale'])->nullable();
            $table->enum('approved_status',['approved','pending','rejected'])->nullable();

            $table->unsignedInteger('approved_by')->nullable();
            $table->foreign('approved_by')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table->string('title')->nullable();
            $table->longText('description')->nullable();
            $table->unsignedInteger('amount')->default(0);
            $table->unsignedInteger('land_area')->default(0);
            $table->dateTime('approved_date')->nullable();
            $table->dateTime('rejected_date')->nullable();
            $table->longText('reason_of_rejection')->nullable();
            $table->enum('ownership',['freehold','leasehold']);
            $table->enum('bidding_type',['free','paid','auction']);
            $table->enum('property_classification',['residential','commercial','industrail', 'agricultural']);
            $table->enum('property_purpose',['forSale','rent']);
            $table->dateTime('auction_start_date')->nullable();
            $table->dateTime('auction_end_date')->nullable();
            $table->unsignedInteger('auction_total_days')->nullable();
            $table->unsignedInteger('no_of_bathrooms')->nullable();
            $table->unsignedInteger('no_of_bedrooms')->nullable();
            $table->string('city')->nullable();
            $table->string('location')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
