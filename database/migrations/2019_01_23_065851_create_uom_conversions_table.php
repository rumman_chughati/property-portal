<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUomConversionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uom_conversions', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('uom_from');
            $table->foreign('uom_from')
                ->references('id')->on('uoms')
                ->onDelete('cascade');

            $table->unsignedInteger('uom_to');
            $table->foreign('uom_to')
                ->references('id')->on('uoms')
                ->onDelete('cascade');

            $table->unsignedInteger('conversion_value');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uom_conversions');
    }
}
