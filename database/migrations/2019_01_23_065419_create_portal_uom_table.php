<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortalUomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portal_uom', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('uom_id');
            $table->foreign('uom_id')
                ->references('id')->on('uoms')
                ->onDelete('cascade');

            $table->unsignedInteger('portal_id');
            $table->foreign('portal_id')
                ->references('id')->on('portals')
                ->onDelete('cascade');

            $table->boolean('public', false)->default(false);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portal_uom');
    }
}
