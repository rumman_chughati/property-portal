<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // factory('App\Users',10)->create();
        $user = \App\User::create([
            'first_name' => 'super',
            'last_name' => 'admin',
            'email' => 'super.admin@therightsw.com',
            'password' => bcrypt(123456),
            // 'avatar' => url('/img/profile-photos/1.png'),
            'gender' => 'male',
            'account_status' => 'approved',
            'token' => bcrypt('super.admin@therightsw.com'),
        ]);
        $user->assignRole('superAdmin');


        $user = \App\User::create([
            'first_name' => 'admin',
            'last_name' => 'admin',
            'email' => 'admin@therightsw.com',
            'password' => bcrypt(123456),
            // 'avatar' => url('/img/profile-photos/1.png'),
            'gender' => 'male',
            'account_status' => 'approved',
            'token' => bcrypt('admin@therightsw.com'),
        ]);
        $user->assignRole('admin');

        $user = \App\User::create([
            'first_name' => 'john',
            'last_name' => 'doe',
            'email' => 'john.doe@therightsw.com',
            'password' => bcrypt(123456),
            // 'avatar' => url('/img/profile-photos/1.png'),
            'gender' => 'male',
            'account_status' => 'approved',
            'token' => bcrypt('john.doe@therightsw.com'),
        ]);
        $user->assignRole('user');


        $user = \App\User::create([
            'first_name' => 'haris',
            'last_name' => 'doe',
            'email' => 'haris.doe@therightsw.com',
            'password' => bcrypt(123456),
            // 'avatar' => url('/img/profile-photos/1.png'),
            'gender' => 'male',
            'account_status' => 'approved',
            'token' => bcrypt('haris.doe@therightsw.com'),
        ]);
        $user->assignRole('dealer');
    }
}
