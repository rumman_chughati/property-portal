<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class FakeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        factory(\App\User::class,1)->create(['account_status'=>'active','email' => 'admin@example.com','gender' => 'male'])->each(function ($user){
            $user->assignRole('superAdmin');
        });

        factory(\App\User::class,5)->create()->each(function ($user){
            $faker = Faker::create();
            $role = $faker->randomElement(['user','dealer']);
            $user->assignRole($role);

            factory(\App\Property::class,2)->create(['user_id'=>$user->id])->each(function ($property){

            });
        });

//        factory(\App\PropertyType::class,1)->create(['name' => 'House'])->each(function ($propertyType){
//            factory(\App\PropertyType::class,1)->create(['name' => 'Flat', 'parent_id' => $propertyType->id])->each(function ($propertySubType) use($propertyType) {
//                factory(\App\Feature::class,1)->create(['name' => 'Rooms','property_type_id'=>$propertySubType->id])->each(function ($feature) use($propertySubType){
//                    $faker = Faker::create();
//                    $feature->value = $faker->randomElement(['0','1']);
//                    $feature->value_type = 'text';
//                    $feature->name = $faker->randomElement(['Dinning Room','Study Room','Laundry Room','Bedroom']);
//                    $feature->property_type_id = $propertySubType->id;
//                    $feature->parent_id = $feature->id;
//                });
//            });
//
//        });

        factory(\App\Region::class,1)->create()->each(function ($region){
            factory(\App\Country::class,1)->create(['region_id'=>$region->id])->each(function ($country){
                factory(\App\State::class,1)->create(['country_id'=>$country->id])->each(function ($state) use($country){
                    factory(\App\City::class,1)->create(['state_id' => $state->id])->each(function ($city) use($state){
                    });
                });
            });
        });

    }
}
