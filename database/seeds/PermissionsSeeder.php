<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;


class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create(['name' => 'edit record']);
        Permission::create(['name' => 'view record']);
        Permission::create(['name' => 'delete record']);
        Permission::create(['name' => 'bid on property']);
        Permission::create(['name' => 'add roles']);
        Permission::create(['name' => 'add permissions']);
        Permission::create(['name' => 'change permissions']);
        Permission::create(['name' => 'add listing']);
        Permission::create(['name' => 'remove listing']);
        Permission::create(['name' => 'edit listing']);
    }
}
