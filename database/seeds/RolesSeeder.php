<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::create(['name' => 'superAdmin']);
        $role->givePermissionTo([
            'edit record','view record', 'delete record',
            'add roles', 'add permissions', 'change permissions', 'add listing',
            'remove listing',  'edit listing'
        ]);

        $role = Role::create(['name' => 'admin']);
        $role->givePermissionTo([
            'edit record','view record', 'delete record',
             'add listing', 'remove listing', 'edit listing'
        ]);

        $role = Role::create(['name' => 'user']);
        $role->givePermissionTo([
             'add listing', 'remove listing', 'edit listing', 'bid on property'
        ]);

        $role = Role::create(['name' => 'dealer']);
        $role->givePermissionTo([
            'add listing', 'remove listing', 'edit listing', 'bid on property'
        ]);
    }
}
