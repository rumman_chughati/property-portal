<?php

use Faker\Generator as Faker;

$factory->define(\App\PropertyType::class, function (Faker $faker) {
    return [
        'name' => $faker->randomElement(['House','Home','Flat']),
    ];
});
