<?php

use Faker\Generator as Faker;

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'email' => $faker->unique()->safeEmail,
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'password' => bcrypt(123456),
        'remember_token' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm',
        'account_status' => $faker->randomElement(['active', 'pending' ,'rejected']),
//        'avatar' => imageUrl($width = 640, $height = 480),
        'gender' => $faker->randomElement(['male', 'female']),
    ];
});
