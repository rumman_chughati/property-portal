<?php

use Faker\Generator as Faker;

$factory->define(App\Property::class, function (Faker $faker) {
    return [
        'user_id' => 7,
        'sold_status' => $faker->randomElement(['sold', 'onSale']),
        'approved_status' => $faker->randomElement(['approved', 'pending', 'rejected']),
        'no_of_bathrooms' => $faker->randomElement(['1','2',3]),
        'no_of_bedrooms' => $faker->randomElement(['1','2','3']),
        'title' => $faker->title,
        'amount' => $faker->numberBetween(10000,20000),
        'land_area' => $faker->numberBetween(10,50),
        'ownership' => $faker->randomElement(['freehold', 'leasehold']),
        'bidding_type' => $faker->randomElement(['free', 'paid', 'auction']),
        'property_purpose' => $faker->randomElement(['forSale', 'rent']),
        'city' => $faker->randomElement(['Rawalpindi','Islamabad','Lahore','Karachi']),
        'location' => $faker->randomElement(['Sadar','I8','Shadbag','Samnabad']),
    ];
});
