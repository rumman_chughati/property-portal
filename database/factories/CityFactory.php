<?php

use Faker\Generator as Faker;

$factory->define(\App\City::class, function (Faker $faker) {
    return [
       "name" => $faker->randomElement(['Islamabad','Rawalpindi','Lahore','Karachi','Peshawar']),
        "telephone_code" => "051",
        "public" => 1,
    ];
});
