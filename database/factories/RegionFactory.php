<?php

use Faker\Generator as Faker;

$factory->define(\App\Region::class, function (Faker $faker) {
    return [
        'region_cd' => "5",
        'name' => "Asia",
        "public" => 1,
    ];
});
