<?php

use Faker\Generator as Faker;

$factory->define(\App\Feature::class, function (Faker $faker) {
    return [
        'name' => $faker->randomElement(['Business And Communication', 'Nearby Locations and Other Facilities','Rooms'
        ,'Healthcare Recreational','Other Facilities']),
        'value' => $faker->randomElement(['0'])
    ];
});
