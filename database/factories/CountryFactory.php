<?php

use Faker\Generator as Faker;

$factory->define(\App\Country::class, function (Faker $faker) {
    return [
        "country_code" => "PK",
        "name" => "Pakistan",
        "country_idd" => 92,
        "country_iso3" => "PAK",
        "public" => 1,
    ];
});
